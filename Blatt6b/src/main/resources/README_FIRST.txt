Hinweis f�r die Oberfl�chengestaltung mit SceneBuilder
******************************************************

Sie m�ssen die Oberfl�che nicht unbedingt mit dem SceneBuilder erstellen.

WENN Sie es tun, verwenden Sie als Controller-Klasse bitte ui.controller.Controller  .

Die Klasse und das Package sind in module-java.info bereits f�r die Verwendung vorbereitet.