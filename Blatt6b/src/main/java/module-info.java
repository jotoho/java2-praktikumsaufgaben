module AufgabeSix {
    exports ui.controller;
    exports upn;
    exports application;
    exports upn.operator;
    exports event;

    requires transitive javafx.controls;
    requires transitive javafx.fxml;

    opens ui.controller to javafx.graphics, javafx.fxml;
}
