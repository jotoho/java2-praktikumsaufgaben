package upn.operator.binary;

import upn.operator.Operator;

import java.util.Stack;

public abstract class BinaryOperator implements Operator {

    public BinaryOperator() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void eval(Stack<Double> stack) {
        if (stack == null) {
            throw new IllegalArgumentException("Illegal null stack");
        }
        if (stack.size() < 2) {
            throw new IllegalArgumentException("Not enough arguments");
        }
        final double xRegister = stack.pop();
        final double yRegister = stack.pop();
        try {
            stack.push(eval(xRegister, yRegister));
        }
        catch (Exception e) {
            stack.push(yRegister);
            stack.push(xRegister);
            throw e;
        }
    }

    protected abstract double eval(double xRegister, double yRegister);
}
