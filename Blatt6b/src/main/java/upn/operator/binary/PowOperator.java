package upn.operator.binary;

public class PowOperator extends BinaryOperator {

    @Override
    protected double eval(double xRegister, double yRegister)
            throws IllegalArgumentException {
        if (Double.isFinite(xRegister) && Double.isFinite(yRegister))
            return Math.pow(yRegister, xRegister);
        else
            throw new IllegalArgumentException("Power parameters need to be finite");
    }
}
