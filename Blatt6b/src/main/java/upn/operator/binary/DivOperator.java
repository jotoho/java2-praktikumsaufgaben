package upn.operator.binary;

public class DivOperator extends BinaryOperator {

    @Override
    protected double eval(double xRegister, double yRegister)
            throws IllegalArgumentException {
        if (Double.isFinite(xRegister) && Double.isFinite(yRegister)) {
            if (xRegister != 0)
                return yRegister / xRegister;
            else
                throw new IllegalArgumentException("xRegister must not be zero");
        }
        else
            throw new IllegalArgumentException("Division parameters need to be finite");
    }
}
