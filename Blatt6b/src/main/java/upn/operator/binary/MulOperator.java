package upn.operator.binary;

public class MulOperator extends BinaryOperator {

    @Override
    protected double eval(double xRegister, double yRegister)
            throws IllegalArgumentException {
        if (Double.isFinite(xRegister) && Double.isFinite(yRegister))
            return xRegister * yRegister;
        else
            throw new IllegalArgumentException("Multiplication parameters need to be finite");
    }
}
