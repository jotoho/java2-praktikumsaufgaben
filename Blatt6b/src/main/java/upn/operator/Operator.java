package upn.operator;

import java.util.Stack;

public interface Operator {
    double EPSILON = 1E-12;

    void eval(Stack<Double> stack);
}
