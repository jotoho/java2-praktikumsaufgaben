package upn.operator.unary;

public class SinOperator extends UnaryOperator {

    @Override
    protected double eval(double xRegister) {
        if (Double.isFinite(xRegister))
            return Math.sin(xRegister);
        else {
            if (Double.isNaN(xRegister))
                throw new ArithmeticException("Parameter passed to SinOperator is NaN");
            else
                throw new ArithmeticException("Parameter passed to SinOperator is infinite");
        }
    }
}
