package event;

/**
 * Interface f�r Klassen, die Daten vom Typ T von {@link EventProvider} erwarten.
 * <p>
 * Interface, muss von das Klassen implementiert werden, um sich {@link EventProvider}-Objekten zum
 * Empfang von {@link Event}-Objkten zu registrieren:
 *
 * @param <T> Datentyp des im {@link Event} gekapselten Objekts.
 *
 * @author M: Faulstich
 */
public interface EventListener<T> {
    /**
     * Diese Methode wird von {@link EventProvider}-Objekten aufgerufen, um Daten vom Typ T zu
     * �bermitteln.
     *
     * @param event Objekt, das das zu �bermittelnde Datenobjekt kapselt.
     */
    void onEvent(Event<T> event);
}
