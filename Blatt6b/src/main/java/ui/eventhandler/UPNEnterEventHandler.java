package ui.eventhandler;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import upn.UPNCore;

public class UPNEnterEventHandler implements EventHandler<MouseEvent> {

    private final UPNCore upnCore;

    public UPNEnterEventHandler(UPNCore upnCore) {
        if (upnCore == null) {
            throw new IllegalArgumentException("UPNCore null");
        }
        this.upnCore = upnCore;
    }

    @Override
    public void handle(MouseEvent arg0) {
        upnCore.enter();
    }
}
