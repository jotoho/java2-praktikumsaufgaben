package ui.eventhandler;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import upn.UPNCore;

public class UPNChangeXYEventHandler implements EventHandler<MouseEvent> {

    private final UPNCore upnCore;

    public UPNChangeXYEventHandler(UPNCore upnCore) {
        if (upnCore == null) {
            throw new IllegalArgumentException("UPNCore null");
        }
        this.upnCore = upnCore;
    }

    @Override
    public void handle(MouseEvent arg0) {
        upnCore.changeXY();
    }
}
