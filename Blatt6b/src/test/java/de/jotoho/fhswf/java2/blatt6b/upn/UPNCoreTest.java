package de.jotoho.fhswf.java2.blatt6b.upn;

import event.Event;
import event.EventListener;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import upn.DefaultUPNCore;
import upn.UPNCore;
import upn.operator.Operator;
import upn.operator.binary.*;
import upn.operator.unary.*;

import java.util.List;

/**
 * JUnit 5 Testklasse zum Testen der {@link UPNCore} Implementierung {@link DefaultUPNCore}.
 *
 * @author M. Faulstich
 */
class UPNCoreTest {

    private Double displayDouble;

    /**
     * EventListener, der den &uuml;ber Event gesandten Anzeigestring in enventString speichert.
     */
    private class DisplayDoubleListener implements EventListener<String> {
        @Override
        public void onEvent(Event<String> event) {
            displayDouble = null;
            try {
                displayDouble = Double.parseDouble(event.getValue());
            }
            catch (NumberFormatException e) {
            }
        }
    }

    /**
     * Test des Additionsoperators.
     */
    @Nested
    @DisplayName("+")
    class AddTest {
        Operator operator = new AddOperator();

        /**
         * Test der Funktion &uuml;ber den Inhalt des Stack.
         */
        @Test
        @DisplayName("3 + 2 = 5")
        void testOkStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            upn.keyIn("3");
            upn.enter();
            upn.keyIn("2");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() >= 1);

            assertEquals(5.0, stack.get(stack.size() - 1));
            assertEquals(5.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator. Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("One operand")
        @Test
        void testOneOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("3");
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Subtraktionsoperator.
     */
    @Nested
    @DisplayName("-")
    class SubTest {
        Operator operator = new SubOperator();

        /**
         * Test der Funktion
         */
        @Test
        @DisplayName("3 - 2 = 1")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("3");
            upn.enter();
            upn.keyIn("2");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);
            assertEquals(1.0, stack.get(stack.size() - 1));
            assertEquals(1.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator.
         * <p>
         * Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("One operand")
        @Test
        void testOneOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("3");
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Multiplikation.
     */
    @Nested
    @DisplayName("*")
    class MulTest {
        Operator operator = new MulOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("3 * 2 = 6")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("3");
            upn.enter();
            upn.keyIn("2");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(6.0, stack.get(stack.size() - 1));
            assertEquals(6.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator. Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("One operand")
        @Test
        void testOneOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("3");
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Division.
     */
    @Nested
    @DisplayName("/")
    class DivTest {
        Operator operator = new DivOperator();

        @Test
        @DisplayName("6 / 2 = 3")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("6");
            upn.enter();
            upn.keyIn("2");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(3.0, stack.get(stack.size() - 1));
            assertEquals(3.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Division durch 0.
         */
        @Test
        @DisplayName("6 / 0")
        void testDiv0() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("6");
                    upn.enter();
                    upn.keyIn("0");
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Division durch -2. Test erkennt fehlerhafte Exception bei fehlerhafter Pr&uuml;fung
         * x < EPSILON.
         */
        @Test
        @DisplayName("6 / -2")
        void testDivNeg() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("6");
            upn.enter();
            upn.keyIn("2");
            upn.changeSign();
            upn.execute(operator);
            assertEquals(1, upn.getStack().size());
            assertEquals(-3.0, upn.getStack().get(0));
            assertEquals(-3.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
        }

        /**
         * Test Stack nach Division durch 0.
         */
        @Test
        @DisplayName("6 / 0 (Stack bleibt unver�ndert)")
        void testDiv0Stack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("6");
            upn.enter();
            upn.keyIn("0");
            try {
                upn.execute(operator);
            }
            catch (Exception e) {
            }
            // Nach der fehlerhaften Operation muss der Stack unver�ndert sein.
            List<Double> stack = upn.getStack();
            assertEquals(2, stack.size());
            assertEquals(0.0, stack.get(stack.size() - 1));
            assertEquals(0.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(6.0, stack.get(stack.size() - 2));
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator. Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("One operand")
        @Test
        void testOneOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("3");
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Potenzfunktion.
     */
    @Nested
    @DisplayName("Y^X")
    class PowTest {
        Operator operator = new PowOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("3 ^ 2 = 9")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("3");
            upn.enter();
            upn.keyIn("2");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(9.0, stack.get(stack.size() - 1));
            assertEquals(9.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator. Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("One operand")
        @Test
        void testOneOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("3");
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test nat&uuml;rlicher Logarithmus.
     */
    @Nested
    @DisplayName("LOG")
    class LogTest {
        Operator operator = new LogOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("ln(2.718281)")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("2");
            upn.keyIn(".");
            upn.keyIn("7");
            upn.keyIn("1");
            upn.keyIn("8");
            upn.keyIn("2");
            upn.keyIn("8");
            upn.keyIn("1");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(1.0, stack.get(stack.size() - 1), 1e-5);
            assertEquals(1.0, displayDouble, 1e-5); // Durch
            // DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.718281, stack.get(stack.size() - 1), 1e-5);
            assertEquals(2.718281, displayDouble, 1e-5); // Durch
            // DisplayDoubleListener
            // aktualisierter Wert
            // der Anzeige.
        }

        /**
         * Test nat&uuml;rlicher Logarithmus 0.
         */
        @Test
        @DisplayName("ln(0.0)")
        void testLn0() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("0");
                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Stack nach natr&uuml;rlichem Logarithmus 0.
         */
        @Test
        @DisplayName("ln(0.0) Stack")
        void testLnStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("0");
            try {
                upn.execute(operator);
            }
            catch (Exception e) {
            }
            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(0.0, stack.get(stack.size() - 1));
            assertEquals(0.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test nat&uuml;rlicher Logarithmus -1E24.
         */
        @Test
        @DisplayName("ln(-1E24)")
        void testLnDoubleMin() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
                    upn.keyIn("1");

                    // 1E3
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E6
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E9
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E12
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E15
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E18
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E21
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    // 1E24
                    upn.keyIn("0");
                    upn.keyIn("0");
                    upn.keyIn("0");

                    upn.enter();

                    List<Double> stack = upn.getStack();
                    assertTrue(stack.size() > 0);

                    assertEquals(1E24, stack.get(stack.size() - 1));
                    assertEquals(1E24, displayDouble); // Durch
                    // DisplayDoubleListener
                    // aktualisierter Wert der
                    // Anzeige.

                    // Vorzeichenwechsel
                    upn.changeSign();

                    upn.execute(operator);
                }
            });
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Sinusfunktion.
     */
    @Nested
    @DisplayName("SIN")
    class SinTest {
        Operator operator = new SinOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("sin (pi/2)")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            // sin pi / 2 = 1
            upn.keyIn("3");
            upn.keyIn(".");
            upn.keyIn("1");
            upn.keyIn("4");
            upn.keyIn("1");
            upn.keyIn("5");
            upn.keyIn("9");
            upn.keyIn("3");
            upn.enter();
            upn.keyIn("2");
            upn.execute(new DivOperator());

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);
            assertEquals(3.141593 / 2, stack.get(stack.size() - 1), 1e-5);
            assertEquals(3.141593 / 2, displayDouble, 1e-5); // Durch
            // DisplayDoubleListener
            // aktualisierter
            // Wert der Anzeige.
            upn.execute(operator);
            stack = upn.getStack();
            assertTrue(stack.size() > 0);
            assertEquals(1.0, stack.get(stack.size() - 1), 1e-5);
            assertEquals(1.0, displayDouble, 1e-5); // Durch
            // DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(3.141593 / 2, stack.get(stack.size() - 1), 1e-5);
            assertEquals(3.141593 / 2, displayDouble, 1e-5); // Durch
            // DisplayDoubleListener
            // aktualisierter
            // Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Kosinus.
     */
    @Nested
    @DisplayName("COS")
    class CosTest {
        Operator operator = new CosOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("cos(0)")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            // cos 0 = 1
            upn.keyIn("0");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(Math.cos(0), stack.get(stack.size() - 1), 1e-5);
            assertEquals(Math.cos(0), displayDouble, 1e-5); // Durch
            // DisplayDoubleListener
            // aktualisierter
            // Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(0, stack.get(stack.size() - 1), 1e-5);
            assertEquals(0, displayDouble, 1e-5); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Tangensfunktion.
     */
    @Nested
    @DisplayName("TAN")
    class TanTest {
        Operator operator = new TanOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("tan(1)")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            // cos 0 = 1
            upn.keyIn("1");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(Math.tan(1), stack.get(stack.size() - 1));
            assertEquals(Math.tan(1), displayDouble); // Durch
            // DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(1, stack.get(stack.size() - 1), 1e-5);
            assertEquals(1, displayDouble, 1e-5); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Quadratwurzel.
     */
    @Nested
    @DisplayName("SQRT")
    class SqrtTest {
        Operator operator = new SqrtOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("sqrt(9)")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("9");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(3.0, stack.get(stack.size() - 1));
            assertEquals(3.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(9, stack.get(stack.size() - 1), 1e-5);
            assertEquals(9, displayDouble, 1e-5); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Invertierung.
     */
    @Nested
    @DisplayName("1/X")
    class InvTest {
        Operator operator = new InvOperator();

        /**
         * Test der Funktion.
         */
        @Test
        @DisplayName("inv(10)")
        void testOk() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.keyIn("0");
            upn.execute(operator);

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(0.1, stack.get(stack.size() - 1));
            assertEquals(0.1, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(10, stack.get(stack.size() - 1), 1e-5);
            assertEquals(10, displayDouble, 1e-5); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit einem Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOP() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.execute(operator);
                }
            });
        }
    }

    /**
     * Test Enter-Funktion.
     */
    @Nested
    @DisplayName("Enter")
    class EnterTest {

        /**
         * Test Eingabemodus.
         * <p>
         * Enter setzt den Rechner in den Funktionsmodus.
         */
        @Test
        @DisplayName("Enter input")
        void testEnterInput() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter();

            List<Double> stack = upn.getStack();
            assertTrue(stack.size() > 0);

            assertEquals(1, upn.getStack().size());
            assertEquals(1, stack.get(stack.size() - 1));
            assertEquals(1, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Funktionsmodus.
         * <p>
         * Enter kopiert den Inhalt des X-Registers.
         */
        @Test
        @DisplayName("Copy x")
        void testEnterX() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter(); // Eingabezeilse -> X
            upn.enter(); // X kopieren
            List<Double> stack = upn.getStack();
            assertEquals(2, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X = 1
            assertEquals(1, displayDouble); // X = 1 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(1, stack.get(stack.size() - 2)); // Y = 1

            upn.keyIn("2");
            upn.enter(); // Eingabezeile -> X
            assertEquals(3, stack.size());
            assertEquals(2, stack.get(stack.size() - 1)); // X = 2
            assertEquals(2, displayDouble); // X = 2 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(1, stack.get(stack.size() - 2)); // Y = 1
            assertEquals(1, stack.get(stack.size() - 3)); // Z = 1
        }

        /**
         * Test Funktionsmodus.
         * <p>
         * Enter ändert lastX nicht.
         */
        @Test
        @DisplayName("LastX unchanged")
        void testEnterLastX() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("2");
            upn.execute(new SinOperator());

            // LastX überprüfen
            upn.lastX();
            assertEquals(2, displayDouble);
            upn.clx();

            upn.enter();

            // LastX unverändert?
            upn.lastX();
            assertEquals(2, displayDouble);
        }
    }

    /**
     * Test L&ouml;schfunktion.
     */
    @Nested
    @DisplayName("CLR")
    class ClrTest {

        /**
         * Test Funtkionsmodus.
         */
        @Test
        @DisplayName("Clear Stack")
        void testClear() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter(); // Eingabezeilse -> X
            upn.enter(); // X kopieren
            upn.keyIn("2");
            upn.enter(); // Eingabezeile -> X
            List<Double> stack = upn.getStack();
            assertEquals(3, stack.size());
            assertEquals(2, stack.get(stack.size() - 1)); // X = 2
            assertEquals(2, displayDouble); // X = 2 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(1, stack.get(stack.size() - 2)); // Y = 1
            assertEquals(1, stack.get(stack.size() - 3)); // Z = 1
            upn.clr();
            assertEquals(0, stack.size());
        }

        /**
         * Pr&uuml;ft, dass im Eingabemodus die Eingabe gel&ouml;scht wird, aber der Stack
         * unver&auml;ndert bleibt.
         */
        @Test
        @DisplayName("Clear Input")
        void testClearInput() {
            // Ein Element auf Stack
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter(); // Eingabezeilse -> X

            // Stack pr�fen
            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X = 2
            assertEquals(1, displayDouble); // X = 2 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // Eingabe f�llen
            upn.keyIn("2");
            upn.keyIn("2");
            upn.keyIn("2");
            upn.keyIn("2");

            // Eingabezeile l�schen
            upn.clr();
            assertEquals(0.0, displayDouble); // X = 2 Durch
            // DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // Eingabe leer?
            assertTrue(upn.getInputString() == null || upn.getInputString().isEmpty());

            // Stack leer?
            assertTrue(stack.isEmpty());
        }
    }

    /**
     * Test X-Register oder Eingabe l&ouml;schen.
     */
    @Nested
    @DisplayName("CLX")
    class ClxTest {

        /**
         * Pr&uuml;ft, dass im Eingabemodus die Eingabe gel&ouml;scht wird, aber der Stack
         * unver&auml;ndert bleibt:
         */
        @Test
        @DisplayName("Clear Input")
        void testClearInput() {
            // Ein Element auf Stack
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter(); // Eingabezeilse -> X

            // Stack pr�fen
            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X = 1
            assertEquals(1, displayDouble); // X = 1 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // Eingabe f�llen
            upn.keyIn("2");
            upn.keyIn("2");
            upn.keyIn("2");
            upn.keyIn("2");

            // Eingabezeile l�schen
            upn.clx();

            // Eingabe leer?
            assertTrue(upn.getInputString() == null || upn.getInputString().isEmpty());

            // Stack noch belegt?
            assertEquals(1, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X = 1
            assertEquals(1, displayDouble); // X = 1 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

        }

        /**
         * Pr&uuml;ft, dass im Funtionsmodus das X-Register im Stack gel&ouml;scht wird.
         */
        @Test
        @DisplayName("Clear X in Stack")
        void testClearStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter(); // Eingabezeilse -> X
            upn.enter(); // X kopieren
            upn.keyIn("2");
            upn.enter(); // Eingabezeile -> X

            // 3 Elemente auf Stack: 2, 1, 1
            List<Double> stack = upn.getStack();
            assertEquals(3, stack.size());
            assertEquals(2, stack.get(stack.size() - 1)); // X = 2
            assertEquals(2, displayDouble); // X = 2 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(1, stack.get(stack.size() - 2)); // Y = 1
            assertEquals(1, stack.get(stack.size() - 3)); // Z = 1

            upn.clx();
            // 2 Elemente auf Stack: 1, 1
            assertEquals(2, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X = 1
            assertEquals(1, displayDouble); // X = 1 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(1, stack.get(stack.size() - 2)); // Y = 1

            upn.clx();
            // 1 Elemente auf Stack: 1
            assertEquals(1, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X = 1
            assertEquals(1, displayDouble); // X = 1 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            upn.clx();
            // 0 Elemente auf Stack: 1
            assertEquals(0, stack.size());
            assertEquals(0.0, displayDouble); // 0.0 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * CLX auf leerem Stack darf keine Exeption werfen.
         */
        @Test
        @DisplayName("Clear X in empty Stack")
        void testClearEmptyStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.clx();
            assertEquals(0.0, displayDouble); // 0.0 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }
    }

    /**
     * Test Vorzeichenwechsel.
     */
    @Nested
    @DisplayName("CHS")
    class ChangeSignTest {

        /**
         * Test Eingabemodus.
         */
        @Test
        @DisplayName("Input")
        void testChsInput() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            upn.keyIn("2");
            upn.enter();

            // Stack vor Vorzeichenwechsel
            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(2, stack.get(stack.size() - 1));
            assertEquals(2, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            upn.keyIn("1");
            // Eingabezeile
            assertEquals("1", upn.getInputString());
            assertEquals(1.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // Stack unver�ndert
            assertEquals(1, stack.size());
            assertEquals(2, stack.get(stack.size() - 1));

            upn.changeSign();
            // Vorzeichen Eingabezeile gewechselt
            assertEquals("-1", upn.getInputString());
            assertEquals(-1.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.
            // Stack unver�ndert
            assertEquals(1, stack.size());
            assertEquals(2, stack.get(stack.size() - 1));

            upn.changeSign();
            // Vorzeichen Eingabezeile gewechselt
            assertEquals("1", upn.getInputString());
            assertEquals(1, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // Stack unver�ndert
            assertEquals(1, stack.size());
            assertEquals(2, stack.get(stack.size() - 1));
        }

        /**
         * Test Funktionsmodus.
         */
        @Test
        @DisplayName("X")
        void testChsX() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");

            assertEquals("1", upn.getInputString());
            assertEquals(1.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            upn.enter();

            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(1.0, stack.get(stack.size() - 1));

            upn.changeSign();
            assertEquals(1, stack.size());
            assertEquals(-1.0, stack.get(stack.size() - 1));
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.changeSign();
                }
            });
        }
    }

    /**
     * Test X<>Y.
     */
    @Nested
    @DisplayName("Change X<>Y")
    class ChangeXYTest {

        @Test
        @DisplayName("Change X Y on Stack")
        public void testStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            // "1"
            upn.keyIn("1");
            upn.enter();

            // "2"
            upn.keyIn("2");
            upn.enter();

            // "3"
            upn.keyIn("3");
            upn.enter();

            List<Double> stack = upn.getStack();

            // Stack vor Tausch
            assertEquals(3, stack.size());
            assertEquals(1, stack.get(stack.size() - 3)); // Z: 1
            assertEquals(2, stack.get(stack.size() - 2)); // Y: 2
            assertEquals(3, stack.get(stack.size() - 1)); // X: 3
            assertEquals(3, displayDouble); // X: 3 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            upn.changeXY();
            assertEquals(3, stack.size());
            assertEquals(1, stack.get(stack.size() - 3)); // Z: 1
            assertEquals(3, stack.get(stack.size() - 2)); // Y: 3
            assertEquals(2, stack.get(stack.size() - 1)); // X: 2
            assertEquals(2, displayDouble); // X: 2 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("Change Input  X Y Stack")
        public void testInputStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            // "1"
            upn.keyIn("1");
            upn.enter();

            // "2"
            upn.keyIn("2");
            upn.enter();

            // ! in Eingabe
            upn.keyIn("3");

            List<Double> stack = upn.getStack();

            // Stack vor Tausch
            assertEquals(2, stack.size());
            assertEquals(1, stack.get(stack.size() - 2)); // Y: 1
            assertEquals(2, stack.get(stack.size() - 1)); // X: 2
            assertEquals("3", upn.getInputString()); // Eingabe: 3
            assertEquals(3.0, displayDouble); // Eingabe: 3 Durch
            // DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            upn.changeXY();
            assertEquals(3, stack.size());
            assertEquals(1, stack.get(stack.size() - 3)); // Z: 1
            assertEquals(3, stack.get(stack.size() - 2)); // Y: 3
            assertEquals(2, stack.get(stack.size() - 1)); // X: 2
            assertEquals(2, displayDouble); // X: 2 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung ohne Operator. Erwartet {@link IllegalArgumentException}.
         */
        @Test
        @DisplayName("No operand")
        void testNoOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.changeXY();
                }
            });
        }

        /**
         * Test Exception bei Ausf&uuml;hrung mit nur einem Element auf dem Stack.
         * <p>
         * Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("One operand on Stack")
        @Test
        void testOneOp() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("1");
                    upn.enter();
                    upn.changeXY();
                }
            });
        }

        /**
         * Test unver&auml;nderter Stack bei Ausf&uuml;hrung mit nur einem Element auf dem Stack.
         */
        @DisplayName("One operand on Stack - Stack unchanged")
        @Test
        void testOneOpStack() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            upn.enter();
            try {
                upn.changeXY();
                fail("Exception expected");
            }
            catch (Exception e) {
                // wird erwartet
            }

            // Stack unver�ndert?
            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(1, stack.get(stack.size() - 1));
            assertEquals(1, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        /**
         * Test Ausf&uuml;hrung mit leerem Stack und Eingabe in Eingabezeile.
         * <p>
         * Erwartet {@link IllegalArgumentException}.
         */
        @DisplayName("InputString and emtpy Stack")
        @Test
        void testOnlyInput() {
            assertThrows(IllegalArgumentException.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    UPNCore upn = new DefaultUPNCore();
                    upn.keyIn("1");
                    upn.changeXY();
                }
            });
        }

        /**
         * Ein Operand auf dem Stack, einer Eingegeben
         */
        @DisplayName("InputString and one value on Stack")
        @Test
        void testOneOnStackAndInput() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            // "1"
            upn.keyIn("1");
            upn.enter();

            // "2"
            upn.keyIn("2");

            List<Double> stack = upn.getStack();

            // Stack vor Tausch
            assertEquals(1, stack.size());
            assertEquals(1, stack.get(stack.size() - 1)); // X: 1

            upn.changeXY();
            assertEquals(2, stack.size());
            assertEquals(2, stack.get(stack.size() - 2)); // Y: 2
            assertEquals(1, stack.get(stack.size() - 1)); // X: 1
            assertEquals(1, displayDouble); // X: 1 Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige

        }
    }

    /**
     * Test Eingabefunktion.
     */
    @Nested
    @DisplayName("Input")
    class InputTest {
        @Test
        @DisplayName("Input")
        void testInput() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            // "1"
            upn.keyIn("1");
            assertEquals("1", upn.getInputString());
            assertEquals(1, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // "2"
            upn.keyIn("2");
            assertEquals("12", upn.getInputString());
            assertEquals(12, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // "3"
            upn.keyIn("3");
            assertEquals("123", upn.getInputString());
            assertEquals(123, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // "."
            upn.keyIn(".");
            assertEquals("123.", upn.getInputString());
            assertEquals(123.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.

            // Vorzeichenwechsel
            upn.changeSign();
            assertEquals("-123.", upn.getInputString());
            assertEquals(-123.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.

            // Vorzeichenwechsel
            upn.changeSign();
            assertEquals("123.", upn.getInputString());
            assertEquals(123.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.

            // "4"
            upn.keyIn("4");
            assertEquals("123.4", upn.getInputString());
            assertEquals(123.4, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der
            // Anzeige.

            // "." (Fehler doppeltes Dezimaltrennzeichen)
            assertThrows(Exception.class, new Executable() {

                @Override
                public void execute()
                        throws Throwable {
                    upn.keyIn(".");
                }
            });
        }

        @Test
        @DisplayName("1")
        void testInput1() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("1");
            assertEquals("1", upn.getInputString());
            assertEquals(1, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("2")
        void testInput2() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("2");
            assertEquals("2", upn.getInputString());
            assertEquals(2, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("3")
        void testInput3() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("3");
            assertEquals("3", upn.getInputString());
            assertEquals(3, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("4")
        void testInput4() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("4");
            assertEquals("4", upn.getInputString());
            assertEquals(4, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("5")
        void testInput5() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("5");
            assertEquals(5, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("6")
        void testInput6() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("6");
            assertEquals("6", upn.getInputString());
            assertEquals(6, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("7")
        void testInput7() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("7");
            assertEquals("7", upn.getInputString());
            assertEquals(7, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("8")
        void testInput8() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("8");
            assertEquals("8", upn.getInputString());
            assertEquals(8, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("9")
        void testInput9() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("9");
            assertEquals("9", upn.getInputString());
            assertEquals(9, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName("0")
        void testInput0() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("0");
            assertEquals("0", upn.getInputString());
            assertEquals(0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }

        @Test
        @DisplayName(".")
        void testInputDecSep() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.keyIn("0");
            upn.keyIn(".");
            assertEquals("0.", upn.getInputString());
            assertEquals(0.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }
    }

    /**
     * Test LastX.
     */
    @Nested
    @DisplayName("LastX")
    class LastXTest {

        /**
         * Ein nicht belegtes LastX Register l&auml;dt 0.0 auf den Stack.
         */
        @Test
        @DisplayName("Init")
        void testInit() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());
            upn.lastX();

            List<Double> stack = upn.getStack();
            assertEquals(1, stack.size());
            assertEquals(0.0, stack.get(stack.size() - 1));
            assertEquals(0.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

        }

        /*
         * Die Funktion von LastX wurde in den Tests der Rechenfunktionen
         * bereits �berpr�ft.
         */

        /**
         * Bei Ausf&uuml;hrung im Eingabemodus muss die Eingabe verworfen und statt dessen der
         * Inhalt des LastX-Registers auf den Stack geschoben werden.
         */
        @Test
        @DisplayName("Input")
        void testInput() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            // "1"
            upn.keyIn("1");
            upn.enter();

            // "2" in Eigabe
            upn.keyIn("2");

            // LastX
            upn.lastX();

            // Erwartet: X: 0, Y: 2, Z:1
            List<Double> stack = upn.getStack();
            assertEquals(3, stack.size());
            assertEquals(0.0, stack.get(stack.size() - 1));
            assertEquals(0.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(2.0, stack.get(stack.size() - 2));
            assertEquals(1.0, stack.get(stack.size() - 3));
        }

        /**
         * Bei Ausf&uuml;hrung im Eingabemodus muss die Eingabe verworfen und statt dessen der
         * Inhalt des LastX-Registers auf den Stack geschoben werden.
         */
        @Test
        @DisplayName("Operation Failure")
        void testOerationFailure() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            // "1"
            upn.keyIn("1");
            upn.enter();

            // "2" in Eigabe
            upn.keyIn("2");

            // Addition
            upn.execute(new AddOperator());

            // LastX
            upn.lastX();

            // Erwartet: X: 2, Y: 3
            List<Double> stack = upn.getStack();
            assertEquals(2, stack.size());
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            assertEquals(3.0, stack.get(stack.size() - 2));

            // In X gespeicherter LastX-Wert l�schen
            upn.clx();

            stack = upn.getStack();
            assertEquals(1.0, stack.size());
            assertEquals(3.0, stack.get(stack.size() - 1));
            assertEquals(3.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.

            // Versuch einer Addition mit nur einem Operanden 3
            try {
                upn.execute(new AddOperator());
            }
            catch (IllegalArgumentException e) {

            }

            // LastX
            upn.lastX();

            // Last X muss den Wert vor der fehlerhaften Operation haben.
            stack = upn.getStack();
            assertEquals(2, stack.size());
            assertEquals(2.0, stack.get(stack.size() - 1));
            assertEquals(3.0, stack.get(stack.size() - 2));
        }
    }

    /**
     * Test des Observer-Patterns.
     */
    @Nested
    @DisplayName("Observer-Pattern")
    class ObserverTest {

        /**
         * Test der Funktion &uuml;ber Display. Wie {@link AddTest#testOkStack()}, die Ergebnisse
         * werden im Anzeigestring &uuml;berpr&uuml;ft.
         */

        @Test
        @DisplayName("Addition")
        void testObserver() {
            UPNCore upn = new DefaultUPNCore(new DisplayDoubleListener());

            // displayString wird zu Double geparst, um unterschiedliche
            // Formatierungen zu ignorieren.
            upn.keyIn("3");
            assertEquals(3.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            upn.enter();
            assertEquals(3.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            upn.keyIn("2");
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            upn.execute(new AddOperator());
            assertEquals(5.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
            // LastX korrekt?
            upn.lastX();
            assertEquals(2.0, displayDouble); // Durch DisplayDoubleListener
            // aktualisierter Wert der Anzeige.
        }
    }
}
