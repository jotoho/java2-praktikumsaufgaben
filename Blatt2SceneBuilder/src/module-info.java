module Blatt2SceneBuilder {
    requires javafx.base;
    requires javafx.controls;
    requires transitive javafx.graphics;
    requires javafx.fxml;

    opens application;
}