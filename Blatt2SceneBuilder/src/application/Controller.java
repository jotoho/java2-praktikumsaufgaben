/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/

package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public class Controller {
    private Label labelForButtonOne = null;
    private Label labelForButtonTwo = null;
    private Label labelForButtonThree = null;
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="button1"
    private Button button1; // Value injected by FXMLLoader

    @FXML // fx:id="button2"
    private Button button2; // Value injected by FXMLLoader

    @FXML // fx:id="button3"
    private Button button3; // Value injected by FXMLLoader

    @FXML // fx:id="stackPane"
    private StackPane stackPane; // Value injected by FXMLLoader

    @FXML
    // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert button1
                != null
            : "fx:id=\"button1\" was not injected: check your FXML file 'CardBorderPane.fxml'.";
        assert button2
                != null
            : "fx:id=\"button2\" was not injected: check your FXML file 'CardBorderPane.fxml'.";
        assert button3
                != null
            : "fx:id=\"button3\" was not injected: check your FXML file 'CardBorderPane.fxml'.";
        assert stackPane
                != null
            : "fx:id=\"stackPane\" was not injected: check your FXML file 'CardBorderPane.fxml'.";
        button1.fire();
    }

    public void buttonOnePressed(ActionEvent event) {
        if (this.labelForButtonOne == null) {
            this.labelForButtonOne = new Label("Panel 1");
            this.stackPane.getChildren().add(this.labelForButtonOne);
        }

        final ObservableList<Node> elementsInContainer = this.stackPane.getChildren();
        elementsInContainer.forEach(element -> element.setVisible(false));

        this.labelForButtonOne.setVisible(true);
        this.labelForButtonOne.toFront();
    }

    public void buttonTwoPressed(ActionEvent event) {
        if (this.labelForButtonTwo == null) {
            this.labelForButtonTwo = new Label("Panel 2");
            this.stackPane.getChildren().add(this.labelForButtonTwo);
        }

        final ObservableList<Node> elementsInContainer = this.stackPane.getChildren();
        elementsInContainer.forEach(element -> element.setVisible(false));

        this.labelForButtonTwo.setVisible(true);
        this.labelForButtonTwo.toFront();
    }

    public void buttonThreePressed(ActionEvent event) {
        if (this.labelForButtonThree == null) {
            this.labelForButtonThree = new Label("Panel 3");
            this.stackPane.getChildren().add(this.labelForButtonThree);
        }

        final ObservableList<Node> elementsInContainer = this.stackPane.getChildren();
        elementsInContainer.forEach(element -> element.setVisible(false));

        this.labelForButtonThree.setVisible(true);
        this.labelForButtonThree.toFront();
    }
}
