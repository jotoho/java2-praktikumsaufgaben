/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * Abgabe f&uuml;r Aufgabenblatt 1.
 *
 * @author Tim Beckmann &lt;beckmann.tim@fh-swf.de&gt;
 * @author Jonas Tobias Hopusch &lt;hopusch.jonastobias@fh-swf.de&gt;
 * @version $Revision: $, $Date: $ UTC
 */
public class Main extends Application {
    /**
     * Startet diese JavaFX Anwendung.
     *
     * @param args
     *           Die Kommandozeilenargumente der Anwendung
     */
    public static void main(final String[] args) {
        launch(args);
    }

    /**
     * Hier wird das HelloWorld Fenster konfiguriert.
     *
     * @param primaryStage
     *           Die Stage vom Fenster
     *
     * @see javafx.application.Application
     */
    @Override
    public void start(final Stage primaryStage) {
        try {
            final BorderPane root = FXMLLoader.load(Main.class.getResource("/CardBorderPane.fxml"));
            final var scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.sizeToScene();
            primaryStage.show();
        } catch (final Throwable e) {
            Alert alert = new Alert(AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.setResizable(true);
            alert.showAndWait();
            //
            Platform.exit();
        }
    }
}