package applicationtest;

import application.Main;
import static java.util.Objects.requireNonNull;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.testfx.api.FxAssert.*;
import static org.testfx.matcher.control.LabeledMatchers.*;
import org.testfx.framework.junit5.ApplicationTest;

import java.net.URL;

public class MainTest extends ApplicationTest {

    public static void main(String[] args)
            throws Exception {
        launch(Main.class,args);
    }

    @Override
    public void start(final Stage primaryStage) {
        try {
            final URL fxmlURL = requireNonNull(MainTest.class.getResource("/UPNBorderPane.fxml"));
            //final URL cssURL = requireNonNull(Main.class.getResource("application.css"));

            final BorderPane root = FXMLLoader.load(fxmlURL);
            final Scene scene = new Scene(root);
            //scene.getStylesheets().add(cssURL.toExternalForm());
            primaryStage.setTitle("RPN-Calculator");
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (Exception e) {
            Alert alert = new Alert(AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
            e.printStackTrace();
        }
    }
    
    
    @Test
    public void buttonOneTest(){
       clickOn("#button1");
       verifyThat("#label", hasText("1"));
    }
    
    @Test
    public void buttonTwoTest(){
       clickOn("#button2");
       verifyThat("#label", hasText("2"));
    }
    
    @Test
    public void buttonThreeTest(){
       clickOn("#button3");
       verifyThat("#label", hasText("3"));
    }
    
    @Test
    public void buttonFourTest(){
       clickOn("#button4");
       verifyThat("#label", hasText("4"));
    }
    
    @Test
    public void buttonFiveTest(){
       clickOn("#button5");
       verifyThat("#label", hasText("5"));
    }
    
    @Test
    public void buttonSixTest(){
       clickOn("#button6");
       verifyThat("#label", hasText("6"));
    }
    
    @Test
    public void buttonSevenTest(){
       clickOn("#button7");
       verifyThat("#label", hasText("7"));
    }
    
    @Test
    public void buttonEightTest(){
       clickOn("#button8");
       verifyThat("#label", hasText("8"));
    }
    
    @Test
    public void buttonNineTest(){
        clickOn("#button9");
        verifyThat("#label", hasText("9"));
    }
    
    @Test
    public void buttonLnTest(){
        clickOn("#button9");
        clickOn("#buttonLn");
        assertEquals(Math.log(9), Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonCosTest(){
        clickOn("#button9");
        clickOn("#buttonCos");
        assertEquals(Math.cos(9), Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonTanTest(){
        clickOn("#button9");
        clickOn("#buttonTan");
        assertEquals(Math.tan(9), Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonSinTest(){
        clickOn("#button9");
        clickOn("#buttonSin");
        assertEquals(Math.sin(9), Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonSqrtTest(){
        clickOn("#button9");
        clickOn("#buttonSqrt");
        assertEquals(Math.sqrt(9), Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonInvTest(){
        clickOn("#button9");
        clickOn("#buttonInv");
        assertEquals(1.0/9.0, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonChsTest(){
        clickOn("#button9");
        clickOn("#buttonChs");
        assertEquals(-9, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
        clickOn("#buttonChs");
        assertEquals(9, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonMulTest(){
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button9");
       clickOn("#buttonMul");
       assertEquals(9.0*9.0, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonDivTest(){
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button9");
       clickOn("#buttonDiv");
       assertEquals(9.0/9.0, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonSubTest(){
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button9");
       clickOn("#buttonSub");
       assertEquals(9.0-9.0, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonAddTest(){
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button9");
       clickOn("#buttonAdd");
       assertEquals(9.0+9.0, Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonDecSep() {
       clickOn("#button9");
       clickOn("#buttonDecSep");
       clickOn("#button9");
       verifyThat("#label", hasText("9.9"));
    }
    
    @Test
    public void buttonChangeXYTest() {
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button2");
       clickOn("#buttonChangeXY");
       verifyThat("#label", hasText("9.0"));
    }
    
    @Test
    public void buttonLastXTest() {
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button2");
       clickOn("#buttonEnter");
       clickOn("#buttonLastX");
       verifyThat("#label", hasText("0.0"));
    }
    
    @Test
    public void buttonPowTest() {
       clickOn("#button8");
       clickOn("#buttonEnter");
       clickOn("#button2");
       clickOn("#buttonPow");
       assertEquals(Math.pow(8, 2), Double.valueOf(((Label)lookup("#label").query()).getText()), 1E-6);
    }
    
    @Test
    public void buttonClrTest() {
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button2");
       clickOn("#buttonEnter");
       clickOn("#buttonClr");
       verifyThat("#label", hasText("0.0"));
    }
    
    @Test
    public void buttonClxTest() {
       clickOn("#button9");
       clickOn("#buttonEnter");
       clickOn("#button2");
       clickOn("#buttonEnter");
       clickOn("#buttonClx");
       verifyThat("#label", hasText("9.0"));
    }
}
