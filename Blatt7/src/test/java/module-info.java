module AufgabeSixTest {
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.controls;
    requires org.junit.jupiter.api;
    requires org.testfx.junit5;
    requires org.testfx;
    requires AufgabeSix;
}