module AufgabeSix {
    exports ui.controller;
    exports upn;
    exports application;
    exports upn.operator;
    exports event;

    requires transitive javafx.controls;
    requires transitive javafx.fxml;
      requires org.junit.jupiter.api;
      requires org.testfx;
      requires org.testfx.junit5;

    opens ui.controller to javafx.graphics, javafx.fxml;
}
