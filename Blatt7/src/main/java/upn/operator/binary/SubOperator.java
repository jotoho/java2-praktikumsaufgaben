package upn.operator.binary;

public class SubOperator extends BinaryOperator {

    @Override
    protected double eval(double xRegister, double yRegister)
            throws IllegalArgumentException {
        if (Double.isFinite(xRegister) && Double.isFinite(yRegister))
            return yRegister - xRegister;
        else
            throw new IllegalArgumentException("Subtraction parameters need to be finite");
    }
}
