package upn.operator.unary;

import upn.operator.Operator;

import java.util.Stack;

public abstract class UnaryOperator implements Operator {

    public UnaryOperator() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void eval(Stack<Double> stack) {
        if (stack == null) {
            throw new IllegalArgumentException("Illegal null stack");
        }
        if (stack.size() < 1) {
            throw new IllegalArgumentException("Not enough arguments");
        }

        final double xRegister = stack.pop();

        try {
            stack.push(eval(xRegister));
        }
        catch (Exception e) {
            stack.push(xRegister);
            throw e;
        }
    }

    protected abstract double eval(double xRegister);
}
