package upn.operator.unary;

public class InvOperator extends UnaryOperator {

    @Override
    protected double eval(double xRegister) {
        if (Double.isNaN(xRegister))
            throw new ArithmeticException("InvOperator cannot invert NaN");
        else
            return 1 / xRegister;
    }
}
