package upn.operator.unary;

public class LogOperator extends UnaryOperator {

    @Override
    protected double eval(double xRegister) {
        if (Double.isNaN(xRegister))
            throw new IllegalArgumentException("Parameter passed to LogOperator is NaN");
        else if (xRegister < (-0.0))
            throw new IllegalArgumentException("Parameter passed to LogOperator is negative");
        else if (xRegister == 0.0)
            throw new IllegalArgumentException("Parameter passed to LogOperator cannot be 0.0");
        else
            return Math.log(xRegister);
    }
}
