package upn.operator.unary;

public class SqrtOperator extends UnaryOperator {

    @Override
    protected double eval(double xRegister) {
        if (Double.isNaN(xRegister))
            throw new ArithmeticException("Parameter passed to SinOperator is NaN");
        else if (xRegister < 0)
            throw new ArithmeticException("Cannot take square root of negative value " + xRegister);
        else
            return Math.sqrt(xRegister);
    }
}
