package upn.operator.unary;

public class TanOperator extends UnaryOperator {

    @Override
    protected double eval(double xRegister) {
        if (Double.isFinite(xRegister))
            return Math.tan(xRegister);
        else {
            if (Double.isNaN(xRegister))
                throw new ArithmeticException("Parameter passed to TanOperator is NaN");
            else
                throw new ArithmeticException("Parameter passed to TanOperator is infinite");
        }
    }
}
