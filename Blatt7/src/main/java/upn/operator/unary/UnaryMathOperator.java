package upn.operator.unary;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class UnaryMathOperator extends UnaryOperator {
    private final String methodName;
    private final Method method;

    public UnaryMathOperator(String methodName) {
        if (methodName == null || methodName.isEmpty()) {
            throw new IllegalArgumentException("Illegal null or empty method");
        }
        this.methodName = methodName;
        try {
            method = java.lang.Math.class.getMethod(methodName, double.class);
        }
        catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(methodName + ": Unsupported Operator");
        }
    }

    @Override
    protected double eval(double xRegister) {
        Double result = null;
        try {
            result = (Double) method.invoke(null, xRegister);
        }
        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new IllegalArgumentException(
                    methodName + ": Failed to invoke " + methodName + "(" + xRegister + ")");
        }
        return result;
    }
}
