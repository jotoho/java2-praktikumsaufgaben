package upn.operator.unary;

public class CosOperator extends UnaryOperator {

    @Override
    protected double eval(double xRegister) {
        if (Double.isFinite(xRegister))
            return Math.cos(xRegister);
        else
            throw new ArithmeticException("Parameter passed to CosOperator is non-finite");
    }
}
