package upn;

import event.DefaultEventProvider;
import event.Event;
import event.EventListener;
import event.EventProvider;
import upn.operator.Operator;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Implementiert den Rechenkern eines UPN-Taschenrechners entsprechend {@link UPNCore}.
 * <p>
 * R�ckmeldungen erfolgen durch Implemenierung von {@link EventProvider}.
 */
public class DefaultUPNCore implements UPNCore, EventProvider<String> {

    private final Stack<Double> stack = new Stack<>();
    private final EventProvider<String> eventProvider = new DefaultEventProvider<>();

    private String inputString = "";

    private double lastX = 0.0;

    /**
     * Initialisierungskonstruktor.
     * <p>
     * Registriert {@link EventListener} als Listener für die {@link EventProvider}
     * Implementierung.
     * <p>
     * Verwendet in Unit-Tests zur Überprüfung der Anzeige.
     *
     * @param eventListener zu registrierender {@link EventProvider}.
     */
    public DefaultUPNCore(EventListener<String> eventListener) {
        this();
        addListener(eventListener);
    }

    /**
     * Defaultkonstruktor.
     */
    public DefaultUPNCore() {

    }

    /**
     * @see EventProvider#addListener(EventListener)
     */
    public void addListener(EventListener<String> listener) {
        eventProvider.addListener(listener);
    }

    /**
     * @see EventProvider#removeListener(EventListener)
     */
    public void removeListener(EventListener<String> listener) {
        eventProvider.removeListener(listener);
    }

    /**
     * @see EventProvider#fireEvent(Event)
     */
    public void fireEvent(Event<String> event) {
        eventProvider.fireEvent(event);
    }

    /**
     * see {@link UPNCore#clr()}
     */
    @Override
    public void clr() {
        this.stack.clear();
        this.inputString = "";
        this.lastX = 0.0;
        this.showLastElement();
    }

    /**
     * @see UPNCore#keyIn(String)
     */
    @Override
    public void keyIn(String s) {
        if (s == null || s.isEmpty())
            throw new IllegalArgumentException("KeyIn string cannot be empty");

        final String newInputString = this.inputString + s;

        if (newInputString.chars().filter(ch -> ch == '.').count() > 1)
            throw new IllegalArgumentException("Number can only contain one decimal divider");

        this.fireEvent(new Event<>(newInputString));
        this.inputString += s;
    }

    /**
     * @see UPNCore#execute(Operator)
     */
    @Override
    public void execute(Operator operator) {
        if (operator == null)
            throw new IllegalArgumentException("Operator cannot be null");

        double tmpLastX = 0.0;
        if (this.isFunctionMode())
            this.enter();
        if (!this.stack.isEmpty())
            tmpLastX = this.stack.lastElement();
        operator.eval(this.stack);

        this.lastX = tmpLastX;
        this.showLastElement();
    }

    /**
     * @see UPNCore#enter()
     */
    @Override
    public void enter() {
        if (!this.isFunctionMode() && !this.stack.isEmpty()) {
            this.stack.push(this.stack.lastElement());
            this.showLastElement();
        }
        else {
            final double doubleInput = Double.parseDouble(this.inputString);
            this.stack.push(doubleInput);
            this.showLastElement();
            this.inputString = "";
        }
    }

    /**
     * @see UPNCore#lastX()
     */
    @Override
    public void lastX() {
        if (this.isFunctionMode())
            this.enter();
        this.stack.push(this.lastX);
        this.showLastElement();
    }

    /**
     * @see UPNCore#changeSign()
     */
    @Override
    public void changeSign() {
        if (this.isFunctionMode()) {
            this.inputString = this.replaceSign(this.inputString);
            this.fireEvent(new Event<>(inputString));
        }
        else if (!this.stack.isEmpty()) {
            String numberToChange = Double.toString(this.stack.pop());
            numberToChange = this.replaceSign(numberToChange);
            this.stack.push(Double.parseDouble(numberToChange));
            this.showLastElement();
        }
        else {
            throw new IllegalArgumentException("Number needed for change sign");
        }
    }

    /**
     * Checks if core is in function mode
     *
     * @return true if core is in function mode, false if not
     */
    public boolean isFunctionMode() {
        return !this.inputString.isEmpty();
    }

    /**
     * Replaces the sign of the input
     */
    protected String replaceSign(String input) {
        if (input == null || input.isEmpty())
            throw new IllegalArgumentException("Input cannot be empty/null");
        if (input.startsWith("-"))
            input = input.replace("-", "");
        else
            input = "-" + input;
        return input;
    }

    /**
     * @see UPNCore#changeXY()
     */
    @Override
    public void changeXY() {
        if (this.isFunctionMode())
            this.enter();
        if (this.stack.size() < 2)
            throw new IllegalArgumentException("At least 2 value are needed to change X and Y");
        double yRegister = this.stack.pop();
        double xRegister = this.stack.pop();

        this.stack.push(yRegister);
        this.stack.push(xRegister);
        this.showLastElement();
    }

    /**
     * @see UPNCore#clx()
     */
    @Override
    public void clx() {
        if (this.isFunctionMode()) {
            this.inputString = "";
            this.showLastElement();
            return;
        }
        if (this.stack.isEmpty()) {
            this.showLastElement();
            return;
        }

        this.stack.pop();
        this.inputString = "";
        this.showLastElement();
    }

    /**
     * @see UPNCore#getStack()
     */
    @Override
    public List<Double> getStack() {
        return Collections.unmodifiableList(this.stack);
    }

    /**
     * @see UPNCore#getInputString()
     */
    @Override
    public String getInputString() {
        return this.inputString;
    }

    /**
     * Shows the last element of the stack If the stack is empty, shows 0.0
     */
    public void showLastElement() {
        if (this.stack.isEmpty())
            this.fireEvent(new Event<>("0.0"));
        else
            this.fireEvent(new Event<>(Double.toString(this.stack.lastElement())));
    }
}
