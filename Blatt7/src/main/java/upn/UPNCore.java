package upn;

import upn.operator.Operator;

import java.util.List;

/**
 * UPN-Rechenkern.
 * <p>
 * Der UPN-Rechnekern ist zust�ndig f�r
 * <ul>
 * <li>den Stack inklusive aller Verwaltungsfunktionen (Enter, XY-Tausch, CLX,
 * etc.)</li>
 * <li>das LastX-Registers,</li>
 * <li>den Eingabetext im Eingabe und Funktionsmodus,</li>
 * <li>den Anzeigetext</li>
 * <li>die korrekte Handhabung des Eingabe- und Funkionsmodus.</li>
 * </ul>
 * <p>
 * Die mathematischen Funktionen sind �ber eine Klassenhierarchie des
 * {@link Operator} Interfaces implementiert und werden in
 * {@link #execute(Operator)} ausgef�hrt.
 *
 * @author M. Faulstich
 */
public interface UPNCore {

    /**
     * Setzt den Rechner in den Funktionsmodus und l�scht den Inhalt des Stacks.
     */
    void clr();

    /**
     * Setzt den Rechner in den Eingabemodus und gibt die �bergebene Ziffer ein.
     *
     * @param input Eingabestring. Zeichen "0" ... "9" und "."
     *
     * @throws IllegalArgumentException bei ung�ltiger Eingabe.
     */
    void keyIn(String input);

    /**
     * Setzt den Rechner in den Funktionsmodus und  f�hrt den �bergebenen Operator aus.
     *
     * @param addOperator Auszuf�hrender Operator.
     *
     * @throws IllegalArgumentException Bei ung�ltiger Anzahl oder ung�ltigem Wert von Operanden.
     */
    void execute(Operator addOperator);

    /**
     * F�hrt Enter aus.
     * <p>
     * Im Eingabemodus wird der Inhalt der Anzeige in Double konvertiert und auf dem Stack gepusht.
     * <p>
     * Im Funktionsmodus wird der letzte auf den Stack geschobene Wert kopiert, aus [1] wird [1,
     * 1].
     */
    void enter();

    /**
     * Setzt den Rechner in den Funktionsmodus und schiebt den Inhalt des LastX-Registers mit push
     * auf den Stack.
     */
    void lastX();

    /**
     * Wechselt das Vorzeichen.
     * <p>
     * Im Eingabemodus wird der Eingabezeile ein negative Vorzeichen vorangestellt oder entfernt.
     * <p>
     * Im Funktionsmodus wird der Wert im X-Register mit -1 multipliziert.a
     */
    void changeSign();

    /**
     * Setzt den Rechner in den Funktionsmodus und tauscht den Inhalt des X- und des Y-Registers.
     */
    void changeXY();

    /**
     * Setzt den Rechner in den Funktionsmodus und l�scht das X-Register des Stacks.
     */
    void clx();

    /**
     * @return Gibt eine nicht ver�nderbare Version des Stack zur�ck.
     */
    List<Double> getStack();

    /**
     * @return Gibt den aktuellen Eingabestring zur�ck.
     */
    String getInputString();
}
