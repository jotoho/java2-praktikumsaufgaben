package event;

/**
 * Interface f�r Klasen, die Datenobjekte vom Typ an registrierte Emfp�nger senden.
 *
 * <p>
 * Interface f�r den Observable-Teil eines Observer Patterns.
 * </p>
 *
 * <ul>
 * <li>Implementierungen von {@link EventListener} registieren sich mit
 * {@link #addListener(EventListener)} am {@link EventProvider}.</li>
 * <li>Bei �nderungen eines Objekts vom Typ T wird dies in einem {@link Event}
 * gekapselt �ber{@link EventProvider#fireEvent(Event)} an alle registrierten
 * {@link EventListener} gesendet.</li>
 * <li>In den {@link EventListener}-Implementierungen wird
 * {@link EventListener#onEvent(Event)} aufgerufen, in dem das Objekt empfangen
 * wird.</li>
 * </ul>
 *
 * @param <T> Datentyp des zu aktualiserenden Wertes:
 *
 * @author M. Faulstich
 */
public interface EventProvider<T> {

    /**
     * @param listener Registriert einen {@link EventListener} zum Datenempfan.
     */
    void addListener(EventListener<T> listener);

    /**
     * @param listener Listener, der vom Empfang abgemeldet werden soll.
     */
    void removeListener(EventListener<T> listener);

    /**
     * Sendet ein {@link Event}, an alle mit {@link #addListener(EventListener)} registierten
     * {@link EventListener}-Objekte.
     *
     * @param event {@link Event}-Objekt, das das ge�nderte Objekt vom Typ T kapselt.
     */
    void fireEvent(Event<T> event);
}
