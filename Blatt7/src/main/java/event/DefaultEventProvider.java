package event;

import java.util.ArrayList;
import java.util.List;

/**
 * Default-Implementierung vpn {@link EventProvider}.
 *
 * <p>
 * Implementationen von {@link EventProvider} eine Objektinstanz dieser Klasse und k�nnen per
 * Delegation die Methoden dieser Klasse implementieren.
 * </p>
 *
 * @param <T> Datentyp des beobachteten Wertes.
 *
 * @author M. Faulstic
 */
public class DefaultEventProvider<T> implements EventProvider<T> {

    /**
     * Liste der registrierten {@link EventListener}.
     */
    private final List<EventListener<T>> listeners = new ArrayList<>();

    /**
     * @see event.EventProvider#addListener(EventListener)
     */
    @Override
    public void addListener(EventListener<T> listener) {
        if (listener != null && !listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    /**
     * @see event.EventProvider#removeListener(EventListener)
     */
    @Override
    public void removeListener(EventListener<T> listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    /**
     * @see event.EventProvider#fireEvent(Event)
     */
    @Override
    public void fireEvent(Event<T> event) {
        for (EventListener<T> listener : listeners) {
            listener.onEvent(event);
        }
    }
}
