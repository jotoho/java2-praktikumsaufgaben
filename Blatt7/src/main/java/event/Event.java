package event;

/**
 * Event-Objekt zum Kapseln eines Objekts vom Typ T, das als Nachricht von {@link EventProvider}
 * �ber {@link EventProvider#fireEvent(Event)} an registrierte {@link EventListener} gesandt
 * werden.
 *
 * @param <T>
 *
 * @author M. Faulstich
 */
public class Event<T> {
    private final T value;

    /**
     * Initialisierungskonstruktor.
     *
     * @param value Datenobject f�r registrierte {@link EventListener}
     */
    public Event(T value) {
        this.value = value;
    }

    /**
     * @return Gibt das im Inititalisierungskonstruktor �bergebenen Objekt zur�ck.
     */
    public T getValue() {
        return value;
    }
}
