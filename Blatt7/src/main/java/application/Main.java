package application;

import static java.util.Objects.requireNonNull;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) {
        try {
            final URL fxmlURL = requireNonNull(Main.class.getResource("/UPNBorderPane.fxml"));
            //final URL cssURL = requireNonNull(Main.class.getResource("application.css"));

            final BorderPane root = FXMLLoader.load(fxmlURL);
            final Scene scene = new Scene(root);
            //scene.getStylesheets().add(cssURL.toExternalForm());
            primaryStage.setTitle("RPN-Calculator");
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (Exception e) {
            Alert alert = new Alert(AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
            e.printStackTrace();
        }
    }
}
