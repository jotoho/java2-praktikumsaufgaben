/**
 * Sample Skeleton for 'UPNBorderPane.fxml' Controller Class
 */

package ui.controller;

import event.Event;
import event.EventListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import ui.eventhandler.*;
import upn.DefaultUPNCore;
import upn.operator.binary.*;
import upn.operator.unary.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="buttonLn"
    private Button buttonLn; // Value injected by FXMLLoader

    @FXML // fx:id="buttonSin"
    private Button buttonSin; // Value injected by FXMLLoader

    @FXML // fx:id="buttonCos"
    private Button buttonCos; // Value injected by FXMLLoader

    @FXML // fx:id="buttonTan"
    private Button buttonTan; // Value injected by FXMLLoader

    @FXML // fx:id="buttonChs"
    private Button buttonChs; // Value injected by FXMLLoader

    @FXML // fx:id="buttonInv"
    private Button buttonInv; // Value injected by FXMLLoader

    @FXML // fx:id="buttonPow"
    private Button buttonPow; // Value injected by FXMLLoader

    @FXML // fx:id="buttonLastX"
    private Button buttonLastX; // Value injected by FXMLLoader

    @FXML // fx:id="buttonChangeXY"
    private Button buttonChangeXY; // Value injected by FXMLLoader

    @FXML // fx:id="buttonClr"
    private Button buttonClr; // Value injected by FXMLLoader

    @FXML // fx:id="button7"
    private Button button7; // Value injected by FXMLLoader

    @FXML // fx:id="button8"
    private Button button8; // Value injected by FXMLLoader

    @FXML // fx:id="button9"
    private Button button9; // Value injected by FXMLLoader

    @FXML // fx:id="buttonDiv"
    private Button buttonDiv; // Value injected by FXMLLoader

    @FXML // fx:id="buttonSqrt"
    private Button buttonSqrt; // Value injected by FXMLLoader

    @FXML // fx:id="buttonClx"
    private Button buttonClx; // Value injected by FXMLLoader

    @FXML // fx:id="button4"
    private Button button4; // Value injected by FXMLLoader

    @FXML // fx:id="button5"
    private Button button5; // Value injected by FXMLLoader

    @FXML // fx:id="button6"
    private Button button6; // Value injected by FXMLLoader

    @FXML // fx:id="buttonMul"
    private Button buttonMul; // Value injected by FXMLLoader

    @FXML // fx:id="button1"
    private Button button1; // Value injected by FXMLLoader

    @FXML // fx:id="button2"
    private Button button2; // Value injected by FXMLLoader

    @FXML // fx:id="button3"
    private Button button3; // Value injected by FXMLLoader

    @FXML // fx:id="buttonSub"
    private Button buttonSub; // Value injected by FXMLLoader

    @FXML // fx:id="button0"
    private Button button0; // Value injected by FXMLLoader

    @FXML // fx:id="buttonDecSep"
    private Button buttonDecSep; // Value injected by FXMLLoader

    @FXML // fx:id="buttonEnter"
    private Button buttonEnter; // Value injected by FXMLLoader

    @FXML // fx:id="buttonAdd"
    private Button buttonAdd; // Value injected by FXMLLoader

    @FXML // fx:id="label"
    private Label label; // Value injected by FXMLLoader

    @FXML
        // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert buttonLn != null
                : "fx:id=\"buttonLn\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert buttonSin != null :
                "fx:id=\"buttonSin\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonCos != null :
                "fx:id=\"buttonCos\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonTan != null :
                "fx:id=\"buttonTan\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonChs != null :
                "fx:id=\"buttonChs\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonInv != null :
                "fx:id=\"buttonInv\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonPow != null :
                "fx:id=\"buttonPow\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonLastX != null :
                "fx:id=\"buttonLastX\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonChangeXY != null :
                "fx:id=\"buttonChangeXY\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonClr != null :
                "fx:id=\"buttonClr\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert button7 != null
                : "fx:id=\"button7\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert button8 != null
                : "fx:id=\"button8\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert button9 != null
                : "fx:id=\"button9\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert buttonDiv != null :
                "fx:id=\"buttonDiv\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonSqrt != null :
                "fx:id=\"buttonSqrt\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonClx != null :
                "fx:id=\"buttonClx\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert button4 != null
                : "fx:id=\"button4\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert button5 != null
                : "fx:id=\"button5\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert button6 != null
                : "fx:id=\"button6\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert buttonMul != null :
                "fx:id=\"buttonMul\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert button1 != null
                : "fx:id=\"button1\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert button2 != null
                : "fx:id=\"button2\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert button3 != null
                : "fx:id=\"button3\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert buttonSub != null :
                "fx:id=\"buttonSub\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert button0 != null
                : "fx:id=\"button0\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        assert buttonDecSep != null :
                "fx:id=\"buttonDecSep\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonEnter != null :
                "fx:id=\"buttonEnter\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert buttonAdd != null :
                "fx:id=\"buttonAdd\" was not injected: check your FXML file 'UPNBorderPane" +
                ".fxml'.";
        assert label != null
                : "fx:id=\"label\" was not injected: check your FXML file 'UPNBorderPane.fxml'.";
        init();
    }

    private void init() {
        DefaultUPNCore upnCore = new DefaultUPNCore();

        upnCore.addListener(new EventListener<String>() {

            @Override
            public void onEvent(Event<String> event) {
                label.textProperty().setValue(event.getValue());
            }
        });

        button1.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "1"));
        button2.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "2"));
        button3.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "3"));
        button4.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "4"));
        button5.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "5"));
        button6.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "6"));
        button7.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "7"));
        button8.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "8"));
        button9.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "9"));
        button0.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNInputEventHandler(upnCore, "0"));

        buttonEnter.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNEnterEventHandler(upnCore));
        buttonLastX.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNLastXEventHandler(upnCore));
        buttonChs.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNChsEventHandler(upnCore));
        buttonChangeXY.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                      new UPNChangeXYEventHandler(upnCore));
        buttonClr.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNClrEventHandler(upnCore));
        buttonClx.addEventFilter(MouseEvent.MOUSE_CLICKED, new UPNClxEventHandler(upnCore));
        buttonDecSep.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                    new UPNInputEventHandler(upnCore, "."));

        buttonLn.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                new UPNOperatorEventHandler(upnCore, new LogOperator()));
        buttonSin.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new SinOperator()));
        buttonCos.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new CosOperator()));
        buttonTan.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new TanOperator()));
        buttonSqrt.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                  new UPNOperatorEventHandler(upnCore, new SqrtOperator()));
        buttonInv.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new InvOperator()));

        buttonAdd.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new AddOperator()));
        buttonSub.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new SubOperator()));
        buttonMul.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new MulOperator()));
        buttonDiv.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new DivOperator()));
        buttonPow.addEventFilter(MouseEvent.MOUSE_CLICKED,
                                 new UPNOperatorEventHandler(upnCore, new PowOperator()));
    }
}
