package ui.eventhandler;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import upn.UPNCore;

import java.util.regex.Pattern;

public class UPNInputEventHandler implements EventHandler<MouseEvent> {

    private static final Pattern INPUT_PATTERN = Pattern.compile("^[0-9.]$");
    private final UPNCore upnCore;
    private final String input;

    public UPNInputEventHandler(UPNCore upnCore, String input) {
        if (upnCore == null) {
            throw new IllegalArgumentException("UPNCore null");
        }
        if (!INPUT_PATTERN.matcher(input).matches()) {
            throw new IllegalArgumentException("Ung�ltge Eingaben: " + input);
        }
        this.upnCore = upnCore;
        this.input = input;
    }

    @Override
    public void handle(MouseEvent arg0) {
        upnCore.keyIn(input);
    }
}
