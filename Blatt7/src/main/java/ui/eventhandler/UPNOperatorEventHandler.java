package ui.eventhandler;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import upn.UPNCore;
import upn.operator.Operator;

public class UPNOperatorEventHandler implements EventHandler<MouseEvent> {

    private final UPNCore upnCore;
    private final Operator operator;

    public UPNOperatorEventHandler(UPNCore upnCore, Operator operator) {
        if (upnCore == null) {
            throw new IllegalArgumentException("UPNCore null");
        }
        if (operator == null) {
            throw new IllegalArgumentException("Operator null");
        }

        this.upnCore = upnCore;
        this.operator = operator;
    }

    @Override
    public void handle(MouseEvent arg0) {
        upnCore.execute(operator);
    }
}
