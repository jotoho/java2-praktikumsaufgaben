plugins {
    java
    id("org.openjfx.javafxplugin") version "0.0.14"
}

group = "de.jotoho.fhswf.java2"
version = "0.0.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter", "junit-jupiter", "[5, 6[")
    testImplementation("org.testfx", "testfx-junit5", "4.0.16-alpha")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

javafx {
    version = "17"
    modules = listOf("javafx.controls", "javafx.fxml")
}

tasks.test {
    useJUnitPlatform()
}
