/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package application;

import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * The purpose of this class is to be fed to the button in the main window to
 * allow them to dynamically update the content in the StackPane to the right of
 * them.
 *
 * To do this, this class is given a reference to the correct stackpane, a copy
 * of the text that is to be shown inside and a reference to the primary stage
 * of the window, in order to be able to dynamically resize it according to the
 * space needs of the new text.
 *
 * @author Tim Beckmann &lt;beckmann.tim@fh-swf.de&gt;
 * @author Jonas Tobias Hopusch &lt;hopusch.jonastobias@fh-swf.de&gt;
 */
public class ButtonEventHandler implements EventHandler<ActionEvent> {
    // the component to the right of the buttons that the new text is to be
    // inserted into
    final StackPane targetContainer;

    // the text to be shown after this button has been triggered
    final String stringForLabel;

    /**
     * A basic constructor
     *
     * @param container the stackpane which is supposed to exclusively hold the
     *     text
     *                  message. Must not be null.
     * @param labelText The text to be displayed inside the container. Must not be
     *                  null
     * @throws NullPointerException If any parameter is null
     */
    public ButtonEventHandler(final StackPane container, final String labelText) {
        this.targetContainer = Objects.requireNonNull(container);
        this.stringForLabel = Objects.requireNonNull(labelText);
    }

    /**
     * The function called by the button once it has been triggered.
     *
     * DO NOT call manually!
     *
     * @param ignore Unused.
     */
    @Override
    public void handle(final ActionEvent ignore) {
        final var elementsInContainer = this.targetContainer.getChildren();
        elementsInContainer.forEach(element -> element.setVisible(false));
        final var existingElementsForThisButton = elementsInContainer.filtered(
                element -> { return ((Label) element).getText().equals(this.stringForLabel); });

        // There should at most be one element that matches the requirements. Warn
        // user if not.
        if (existingElementsForThisButton.size() > 1)
            new Alert(Alert.AlertType.WARNING,
                    "Warning: Something unexpected happened while updating UI after button activation. "
                            + "You may continue to use the software but please consider informing the developers.");

        // If there's none, make a new one.
        if (existingElementsForThisButton.size() == 0)
            elementsInContainer.add(new Label(this.stringForLabel));
        // Reuse the old one
        else {
            final var chosenLabel = existingElementsForThisButton.get(0);
            chosenLabel.toFront();
            chosenLabel.setVisible(true);
        }
    }
}
