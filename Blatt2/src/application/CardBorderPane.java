/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package application;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * This component consists of a bar along the left size that allows the user to
 * switch between different texts for the center. The buttons and their
 * respective text is hardcoded due to the simple nature of this project.
 *
 * @author Tim Beckmann &lt;beckmann.tim@fh-swf.de&gt;
 * @author Jonas Tobias Hopusch &lt;hopusch.jonastobias@fh-swf.de&gt;
 */
public class CardBorderPane extends BorderPane {
    // Used to store data required to configure one of the buttons
    // in an organized way
    private record ButtonData(String buttonName, String labelText) {}

    /**
     * Basic constructor for this component
     */
    public CardBorderPane() {
        // Prepare the container for the buttons
        final VBox buttonBox = new VBox();
        buttonBox.setAlignment(Pos.CENTER);
        // all buttons should be the same width
        buttonBox.setFillWidth(true);
        this.setLeft(buttonBox);

        // Prepare the container for the text to be displayed
        final StackPane labelBox = new StackPane();
        this.setCenter(labelBox);

        // Data used to populate the list of buttons
        // Used to avoid "Don't repeat yourself" rule violations
        final ButtonData[] buttonData = {new ButtonData("Button 1", "Panel 1"),
                new ButtonData("Button 2", "Panel 2"), new ButtonData("Button 3", "Panel 3")};

        // Generate a button for every entry in buttonData
        // Using a loop instead of duplicating code is good style
        for (final ButtonData currentButtonData : buttonData) {
            // the event handler for this specific button
            final var buttonEventHandler =
                    new ButtonEventHandler(labelBox, currentButtonData.labelText);
            final var button = new Button(currentButtonData.buttonName);
            button.addEventHandler(ActionEvent.ACTION, buttonEventHandler);
            // to allow the button to be resized to the size of the box
            button.setMaxWidth(Double.MAX_VALUE);
            buttonBox.getChildren().add(button);
        }

        if (buttonData.length > 0)
            if (buttonBox.getChildren().get(0) instanceof Button firstButton) firstButton.fire();
    }
}
