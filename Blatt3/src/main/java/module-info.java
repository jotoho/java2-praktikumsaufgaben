module Java2_RegexChecker {
    requires transitive java.desktop;
    requires transitive javafx.graphics;
    requires transitive javafx.controls;

    opens de.jotoho.fhswf.java2.Blatt3 to javafx.graphics;
    opens de.fhswf.fbin.java2fx.validation to javafx.graphics;
}
