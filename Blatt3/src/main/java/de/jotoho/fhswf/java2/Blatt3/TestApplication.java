package de.jotoho.fhswf.java2.Blatt3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * Creates Window containing {@link TextCheckerVBox} that checks if the text is a valid email.
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class TestApplication extends Application {

    /**
     * This main method immediately delegates control to JavaFX
     *
     * @param args The CLI arguments to be passed to JavaFX
     */
    public static void main(final String[] args) {
        launch(args);
    }

    /**
     * Creates the window containing the text field and the result label.
     *
     * <p>It also decides which regex to match against (usually hardcoded)</p>
     *
     * @param primaryStage Reference to stage passed by JavaFX
     * @see Application#start(Stage)
     */
    @Override
    public void start(final Stage primaryStage) {
        Objects.requireNonNull(primaryStage);

        final var scene = new Scene(new TextCheckerVBox(new EmailTextField()));

        primaryStage.setScene(scene);
        primaryStage.setMinHeight(20);
        primaryStage.setMinWidth(20);
        primaryStage.sizeToScene();
        primaryStage.setTitle("RegEx Testapplication");
        primaryStage.show();
    }
}
