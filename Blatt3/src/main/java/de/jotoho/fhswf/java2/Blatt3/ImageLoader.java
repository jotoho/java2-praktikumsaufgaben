package de.jotoho.fhswf.java2.Blatt3;

import javafx.scene.control.Alert;
import javafx.scene.image.Image;

import java.util.MissingResourceException;
import java.util.Objects;
import java.util.Optional;

/**
 * This class loads and provides access to image resources
 *
 * <p>
 * This class represents a static collection of Images/Icons that can be
 * loaded into memory using {@link #load} and then accessed via {@link #get}.
 * </p>
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public enum ImageLoader {
    // NAME_OF_IMAGE(RESOURCE PATH TO IMAGE)
    ERROR_ICON("/img/error.png");

    // The resource path of this specific image
    private final String resourcePath;

    // The image when loaded into memory or null
    // Potential improvement: Usage of java.lang.ref.SoftReference<T>
    private Image loadedImage = null;

    /**
     * Saves path from enum declaration to internal attribute.
     *
     * <p>This function is <strong>private</strong></p>
     *
     * @param path The resource path for {@link Class#getResource(String)}. Must not be null
     * @throws NullPointerException For naughty devs that pass a null despite my warning.
     */
    ImageLoader(final String path) {
        this.resourcePath = Objects.requireNonNull(path);
    }

    /**
     * Loads image corresponding to this instance into memory, if possible.
     *
     * <p>
     * Calling this function, while the image is already loaded does nothing.
     * </p>
     *
     * @return A reference to this instance (for method call chaining)
     * @throws MissingResourceException If the configured resource path does not point at a valid
     *                                  file.
     * @throws IllegalArgumentException If something else goes wrong while loading the image.
     */
    public synchronized ImageLoader load() throws MissingResourceException,
                                                  IllegalArgumentException {
        if (this.loadedImage == null) {
            final var
                resourceURL =
                this
                    .getClass()
                    .getResource(this.resourcePath);

            // If resource does not exist
            if (resourceURL == null) {
                new Alert(Alert.AlertType.ERROR, "Image '" + this.resourcePath + "' could not be " +
                                                 "found. This usually indicates a broken " +
                                                 "installation or a major software bug. Please " +
                                                 "contact your system administrator.").showAndWait();
                throw new MissingResourceException("Image '" + this.resourcePath + "' could not " +
                                                   "be found.",
                                                   this
                                                       .getClass()
                                                       .getName(),
                                                   this.resourcePath);
            }

            try {
                this.loadedImage = new Image(resourceURL.toString());
            } catch (final IllegalArgumentException e) {
                new Alert(Alert.AlertType.ERROR,
                          "Internal URL for image loading (" + resourceURL +
                          ") was invalid " +
                          "or " +
                          "unusable. This indicates a broken software " +
                          "installation or a major software bug. Please " +
                          "contact your system administrator.");
                throw e;
            }
        }

        return this;
    }

    /**
     * Checks if the image is currently loaded into memory.
     *
     * @return true if loaded, false if not.
     */
    public synchronized boolean loaded() {
        return this.loadedImage != null;
    }

    /**
     * Returns an {@link Optional} containing the previously loaded image
     * or an empty Optional if not loaded.
     *
     * @return see above
     */
    public Optional<Image> get() {
        return Optional.ofNullable(this.loadedImage);
    }
}
