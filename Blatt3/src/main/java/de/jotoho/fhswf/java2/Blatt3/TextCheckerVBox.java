package de.jotoho.fhswf.java2.Blatt3;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.util.Objects;

/**
 * This component displays the given {@link RegExTextField} and provides feedback
 * to the user.
 *
 * <p>The Feedback is given via a Label placed beneath the RegexTextField.</p>
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class TextCheckerVBox extends VBox {
    // Text to be displayed in the label when the input is either valid or invalid
    private static final String LABELTEXT_VALID = "Content is valid";
    private static final String LABELTEXT_INVALID = "Content is invalid";

    /**
     * Takes the {@link RegExTextField} that's supposed to be used for
     * validation and links it to a feedback label.
     *
     * <p>
     * Please note that {@link ImageLoader} is used to supply the image(s) used with the
     * label and IllegalArgumentExceptions and MissingResourceExceptions from it may be
     * passed on to the caller of this function, if something goes wrong during image retrieval.
     * </p>
     *
     * @param filteringTextField The text field that will receive and verify the
     *                           text by the application user. Must not be null.
     * @throws NullPointerException               If Parameter filteringTextField is null.
     * @throws IllegalArgumentException           see above
     * @throws java.util.MissingResourceException see above
     */
    public TextCheckerVBox(final RegExTextField filteringTextField) {
        Objects.requireNonNull(filteringTextField);

        this
            .getChildren()
            .add(filteringTextField);

        final var label = new Label("", null);
        this
            .getChildren()
            .add(label);

        // Fires whenever the contents of the text field become valid or invalid
        filteringTextField.setOnStatusChange(event -> {
            if (event.isStatus()) {
                // Content is valid
                label.setGraphic(null);
                label.setText(LABELTEXT_VALID);
            } else {
                // Content is invalid
                label.setGraphic(new ImageView(ImageLoader.ERROR_ICON
                                                   .load()
                                                   .get()
                                                   .orElse(null)));
                label.setText(LABELTEXT_INVALID);
            }
        });
    }
}
