package de.jotoho.fhswf.java2.Blatt3;

import de.fhswf.fbin.java2fx.validation.StatusListener;
import javafx.scene.control.TextField;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * TextField that matches the user input against a regex pattern given to the constructor.
 *
 * <p>
 * It allows the user of this class to set one event listener that will
 * be notified if the contents of this text field become valid or invalid
 * and receive the current state.
 * </p>
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class RegExTextField extends TextField {
    // This component checks against regex and keep track of the current status
    // to dispatch events to the listener
    private final RegExValidator validator;

    // The listener currently registered to receive events from this text field or null if none.
    private StatusListener<String> listener = null;

    /**
     * Compiles the given regex pattern and does necessary setup work.
     *
     * @param regex The regular expression to match against the input of the user. Must not be null.
     * @throws NullPointerException if the user passes a null parameter anyway
     */
    public RegExTextField(final String regex) {
        final var regexPattern = Pattern.compile(Objects.requireNonNull(regex));
        this.validator = new RegExValidator(regexPattern);
        this
            .textProperty()
            .addListener(this.validator);
    }

    /**
     * Sets or replaces the listener receiving updates to the regex matching status.
     *
     * <p>
     * Please note, that there can only be one listener registered to this TextField at a time.
     * Supplying a new listener while one is already registered replaces the previous one.
     * Passing null as the parameter unregisters the previous listener (if any).
     * </p>
     *
     * @param newListener The new listener or null to disable events
     */
    public synchronized void setOnStatusChange(final StatusListener<String> newListener) {
        if (this.listener != null)
            this.validator.removeStatusListener(this.listener);

        if (newListener != null) {
            this.listener = newListener;
            this.validator.addStatusListener(newListener);
        }
    }
}
