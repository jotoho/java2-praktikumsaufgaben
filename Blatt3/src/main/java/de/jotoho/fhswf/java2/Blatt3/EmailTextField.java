package de.jotoho.fhswf.java2.Blatt3;

/**
 * A specialization of {@link RegExTextField} that uses a hardcoded regex
 * that matches some but not necessarily all email addresses.
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class EmailTextField extends RegExTextField {
    // Regex from https://www.baeldung.com/java-email-validation-regex
    // I had trouble getting the one provided inside the task assignment to work
    private static final String
        EMAIL_REGEX =
        "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\" +
        ".[A-Za-z]{2,})$";

    /**
     * Simply calls the constructor of the parent class with the hardcoded
     * regex pattern for email addresses.
     *
     * @see RegExTextField#RegExTextField(String)
     */
    public EmailTextField() {
        super(EMAIL_REGEX);
    }
}
