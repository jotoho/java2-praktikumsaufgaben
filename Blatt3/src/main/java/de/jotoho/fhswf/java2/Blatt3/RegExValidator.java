package de.jotoho.fhswf.java2.Blatt3;

import de.fhswf.fbin.java2fx.validation.ValidationAdapter;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Specialized Validator that checks the given values against
 * the regex pattern provided during construction.
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class RegExValidator extends ValidationAdapter<String> {
    private final Pattern regexPattern;

    /**
     * Basic constructor
     *
     * @param regexPattern The compiled pattern to match against.
     */
    public RegExValidator(final Pattern regexPattern) {
        this.regexPattern = Objects.requireNonNull(regexPattern);
    }

    /**
     * Checks the newest text value against the internal regex pattern.
     *
     * @param value The string to match
     * @return Whether the pattern matches the given value
     * @see ValidationAdapter#isValid
     */
    @Override
    public boolean isValid(final String value) {
        Objects.requireNonNull(value);
        return regexPattern
            .matcher(value)
            .matches();
    }
}
