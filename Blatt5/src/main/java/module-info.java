module BlattFive {
    requires transitive javafx.controls;

    opens de.jotoho.fhswf.java2.Blatt5 to javafx.graphics;
    opens de.jotoho.fhswf.java2.Blatt5.menu to javafx.graphics;
    opens de.jotoho.fhswf.java2.Blatt5.directoryviewer to javafx.graphics;
    opens de.jotoho.fhswf.java2.Blatt5.menu.menuitem to javafx.graphics;
}
