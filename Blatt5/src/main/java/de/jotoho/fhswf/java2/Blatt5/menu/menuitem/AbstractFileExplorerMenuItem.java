/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt5.menu.menuitem;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;

/**
 * This class serves as a template for menu items.
 *
 * @author Tim Beckmann {@literal <beckmann.tim@fh-swf.de>}
 */
public abstract class AbstractFileExplorerMenuItem extends MenuItem implements EventHandler<ActionEvent> {
    /**
     * @param name The text that is displayed in the UI
     * @throws NullPointerException If name parameter is null.
     */
    protected AbstractFileExplorerMenuItem(final String name) {
        if (name == null) throw new NullPointerException("Name for MenuItem cannot be null");
        this.setText(name);
        this.setMnemonicParsing(true);
        this.setOnAction(this);
    }

    /**
     * @param name The text that is displayed in the UI
     * @param key  The accelerator key that should be used for this Item
     * @throws NullPointerException If name or key parameter is null.
     */
    protected AbstractFileExplorerMenuItem(final String name, final KeyCombination key) {
        this(name);
        if (key == null) throw new NullPointerException("KeyCombination for MenuItem cannot be null");
        this.setAccelerator(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            doOnAction();
        } catch (final Throwable e) {
            final Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.setResizable(true);
            alert.showAndWait();
        }
    }

    /**
     * This method has to be overridden by the child class.
     * The actions that should be performed whenever the item is pressed have to be implemented in this method.
     */
    public abstract void doOnAction();
}
