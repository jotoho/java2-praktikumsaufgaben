/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt5;

import de.fhswf.fbin.java2fx.trees.DirectoryTreeView;
import de.jotoho.fhswf.java2.Blatt5.directoryviewer.DirectoryChangeListener;
import de.jotoho.fhswf.java2.Blatt5.directoryviewer.DirectoryTableView;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;

/**
 * This component allows the user to select any directory on the system
 * on the left side and have the files contained in that directory be
 * listed on it's right side.
 *
 * <p>
 * The two sides can be resized by the user at-will.
 * </p>
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class FileExplorerSplitPane extends SplitPane {
    public FileExplorerSplitPane() {
        final ObservableList<Node> items = this.getItems();

        // the directories shown on the left side
        final var dirTreeView = new DirectoryTreeView();
        items.add(dirTreeView);

        // the files of the selected directory
        final var dirTableView = new DirectoryTableView();
        // Set the listener that notifies the tableview when the directory has
        // to change
        dirTreeView
            .selectionModelProperty()
            .get()
            .selectedItemProperty()
            .addListener(new DirectoryChangeListener(dirTableView));
        items.add(dirTableView);
    }
}
