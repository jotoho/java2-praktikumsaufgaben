/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt5.directoryviewer;

import de.fhswf.fbin.java2fx.entities.FXFile;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Alert;
import javafx.scene.control.TreeItem;

import java.io.File;
import java.util.Objects;

/**
 * This listener is designed to notify the {@link DirectoryTableView}
 * whenever the directory currently selected by
 * {@link de.fhswf.fbin.java2fx.trees.DirectoryTreeView}
 * changes and pass it the new path as an instance of {@link FXFile}.
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class DirectoryChangeListener implements ChangeListener<TreeItem<FXFile>> {
    private final DirectoryTableView fileView;

    /**
     * @param fileView The {@link DirectoryTableView} to be notified of its new
     *                 directory to display.
     */
    public DirectoryChangeListener(final DirectoryTableView fileView) {
        this.fileView = Objects.requireNonNull(fileView);
    }

    /**
     * Called whenever the selected directory changes.
     * It sends the new directory to the {@link DirectoryTableView} passed during
     * listener construction.
     *
     * @param observable unused.
     * @param oldValue   unused.
     * @param newValue   The item containing the new directory path. Must be non-null and
     *                   contain a valid directory path (the directory must exist).
     */
    @Override
    public void changed(final ObservableValue<? extends TreeItem<FXFile>> observable,
                        final TreeItem<FXFile> oldValue,
                        final TreeItem<FXFile> newValue) {
        try {
            this.changeDirectory(newValue.getValue().getFile());
        } catch (final Throwable throwable) {
            final String errorMsg = "Something went wrong while attempting to switch " +
                                    "directory\n\n" + throwable.getMessage();
            new Alert(Alert.AlertType.ERROR, errorMsg).show();
        }
    }

    private void changeDirectory(final File file) {
        this.fileView.changeDirectory(Objects.requireNonNull(file));
    }
}
