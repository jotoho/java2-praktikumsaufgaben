/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt5.directoryviewer;

import de.fhswf.fbin.java2fx.entities.FXFile;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Objects;

/**
 * Shows all files contained within the directory it is currently instructed
 * to display.
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 */
public class DirectoryTableView extends TableView<FXFile> {
    private File currentDirectory;

    /**
     * If null is passed, this component begins with an empty list of files.
     * Otherwise, it loads the list of files in the given directory.
     *
     * @param startingDirectory A valid directory matching the requirements of
     *                          {@link #changeDirectory(File)}
     *                          or null.
     */
    public DirectoryTableView(final File startingDirectory) {
        if (startingDirectory != null)
            this.changeDirectory(startingDirectory);

        // Sum of all column widths must match width of TableView
        this.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // Define the columns to show
        this.initializeColumns();
    }

    /**
     * Calls {@link #DirectoryTableView(File)} with null as parameter.
     */
    public DirectoryTableView() {
        this(null);
    }

    private void initializeColumns() {
        // reset list of columns, if called before
        if (!this
            .getColumns()
            .isEmpty()) {
            this
                .getColumns()
                .clear();
        }

        // file name
        {
            final var fileNameColumn = new TableColumn<FXFile, String>("name");
            fileNameColumn.setCellValueFactory(data -> data
                .getValue()
                .nameProperty());
            this
                .getColumns()
                .add(fileNameColumn);
        }

        // File length
        {
            final var fileLengthColumn = new TableColumn<FXFile, Number>("length");
            fileLengthColumn.setCellValueFactory(data -> data
                .getValue()
                .lengthProperty());
            this
                .getColumns()
                .add(fileLengthColumn);
        }

        // Last modified
        {
            final var
                fileLastModifiedColumn =
                new TableColumn<FXFile, String>("last modified");
            fileLastModifiedColumn.setCellValueFactory(data -> {
                final var timestamp = data
                    .getValue()
                    .getLastModified();

                final String timeString = DateTimeFormatter.ISO_LOCAL_DATE_TIME
                    .format(timestamp)
                    .replace('T', ' ');

                return new SimpleStringProperty(timeString);
            });
            this
                .getColumns()
                .add(fileLastModifiedColumn);
        }
    }

    /**
     * Updates/reloads the list of files.
     */
    public void update() {
        final var currentDir = this.currentDirectory;

        if (currentDir != null) {
            final File[] containedFiles =
                Objects.requireNonNullElse(this.currentDirectory.listFiles(),
                                           new File[0]);
            final FXFile[]
                fxFiles =
                Arrays
                    .stream(containedFiles)
                    .parallel()
                    .filter(File::isFile)
                    .map(FXFile::new)
                    .toArray(FXFile[]::new);
            this
                .getItems()
                .setAll(fxFiles);
        } else {
            this
                .getItems()
                .clear();
        }
    }

    /**
     * Change the directory, whose content is displayed.
     * This method also updates the list of files.
     *
     * @param dirPath The path of the directory to list the files of. Must be an existing directory
     *                and not null.
     * @throws NullPointerException     If dirPath is null.
     * @throws IllegalArgumentException If dirPath does not refer to a valid directory
     */
    public void changeDirectory(final File dirPath) {
        Objects.requireNonNull(dirPath);

        if (!dirPath.exists())
            throw new IllegalArgumentException("Directory passed to DirectoryTableView does not " +
                                               "exist");

        if (!dirPath.isDirectory())
            throw new IllegalArgumentException("Path passed to DirectoryTableView does not point" +
                                               "at a directory");

        this.currentDirectory = dirPath;
        this.update();
    }
}
