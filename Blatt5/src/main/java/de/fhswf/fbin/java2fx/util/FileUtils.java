package de.fhswf.fbin.java2fx.util;

import java.io.File;

public final class FileUtils
{
   private FileUtils()
   {
   }

   public static boolean isPublicDirectory(File dir)
   {
      SecurityManager security = System.getSecurityManager();
      boolean isReadable = true;
      if (security != null)
      {
         try
         {
            security.checkRead(dir.toURI().toString());
         }
         catch (Exception e)
         {
            isReadable = false;
         }
      }

      return dir.isDirectory() && isReadable && !dir.isHidden();
   }
}
