plugins {
    java
    application
    id("org.openjfx.javafxplugin") version "0.0.+"
}

group = "de.jotoho.fhswf.java2"
version = "0.0.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter", "junit-jupiter", "[5, 6[")
    // implementation("org.openjfx", "javafx-controls", "[17, 18[")
}

application {
    mainClass.set("de.jotoho.fhswf.java2.Blatt5.TestApplication")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

javafx {
    version = "17"
    modules = listOf("javafx.controls")
}

tasks.test {
    useJUnitPlatform()
}
