plugins {
    java
    id("org.openjfx.javafxplugin") version "0.0.+"
}

group = "de.jotoho.fhswf.java2"
version = "0.0.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter", "junit-jupiter", "[5, 6[")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

javafx {
    version = "17"
    modules = listOf("javafx.controls")
}

tasks.test {
    useJUnitPlatform()
}
