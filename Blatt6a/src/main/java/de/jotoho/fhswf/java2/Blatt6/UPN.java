/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt6;

import de.jotoho.fhswf.java2.Blatt6.operators.BinaryOperator;
import de.jotoho.fhswf.java2.Blatt6.operators.Operator;
import de.jotoho.fhswf.java2.Blatt6.operators.UnaryOperator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * This class calculates the result from a upnExpression.
 */
public class UPN {
    // Unmodifiable
    private static final Map<String, Operator> operators;
    private static final Map<String, Predicate<double[]>> operatorValidators;
    private static final Pattern SPLIT_PATTERN = Pattern.compile("[\\p{Space}]+");

    static {
        operatorValidators = initializeValidators();
        final ConcurrentMap<String, Operator> operatorMap = new ConcurrentHashMap<>();
        initializeBinaryOperators(operatorMap);
        initializeUnaryOperators(operatorMap);
        initializeBasicOps(operatorMap);
        operators = Collections.unmodifiableMap(operatorMap);
    }

    /**
     * This method initializes the operator that use symbols.
     *
     * @param opsMap The map that later should contain the operators
     */
    private static void initializeBasicOps(final Map<String, Operator> opsMap) {
        opsMap.putIfAbsent("+", new BinaryOperator() {
            @Override
            protected double eval(final double a, final double b)
                    throws IllegalArgumentException {
                if (Double.isFinite(a) && Double.isFinite(b))
                    return a + b;
                else
                    throw new IllegalArgumentException("Addition parameters need to be finite");
            }
        });
        opsMap.putIfAbsent("-", new BinaryOperator() {
            @Override
            protected double eval(final double a, final double b)
                    throws IllegalArgumentException {
                if (Double.isFinite(a) && Double.isFinite(b))
                    return a - b;
                else
                    throw new IllegalArgumentException("Subtraction parameters need to be finite");
            }
        });
        opsMap.putIfAbsent("*", new BinaryOperator() {
            @Override
            protected double eval(final double a, final double b)
                    throws IllegalArgumentException {
                if (Double.isFinite(a) && Double.isFinite(b))
                    return a * b;
                else
                    throw new IllegalArgumentException(
                            "Multiplication parameters need to be " + "finite");
            }
        });
        opsMap.putIfAbsent("/", new BinaryOperator() {
            @Override
            protected double eval(final double a, final double b)
                    throws IllegalArgumentException {
                if (Double.isFinite(a) && Double.isFinite(b)) {
                    if (b != 0)
                        return a / b;
                    else
                        throw new ArithmeticException("b must not be zero");
                }
                else
                    throw new IllegalArgumentException("Division parameters need to be finite");
            }
        });
        opsMap.putIfAbsent("%", new BinaryOperator() {
            @Override
            protected double eval(final double a, final double b)
                    throws IllegalArgumentException {
                if (Double.isFinite(a) && Double.isFinite(b)) {
                    if (b != 0)
                        return a % b;
                    else
                        throw new ArithmeticException("b must not be zero");
                }
                else
                    throw new IllegalArgumentException("Modulo parameters need to be finite");
            }
        });
    }

    /**
     * This method fills the ruleMap with rules.
     *
     * @return the filled ruleMap.
     */
    private static Map<String, Predicate<double[]>> initializeValidators() {
        final var ruleMap = new HashMap<String, Predicate<double[]>>();
        // Keys have to be completely lower case
        ruleMap.put("sqrt", arr -> arr[0] >= 0);
        ruleMap.put("cbrt", arr -> arr[0] >= 0);
        return Collections.unmodifiableMap(ruleMap);
    }

    /**
     * This method initializes the unary operators.
     *
     * @param opsMap The map that later should contain the operators
     */
    private static void initializeUnaryOperators(final ConcurrentMap<String, Operator> opsMap) {
        Arrays.stream(Math.class.getMethods())
              .filter(m -> Modifier.isStatic(m.getModifiers()))
              .filter(m -> Arrays.equals(new Class[]{double.class}, m.getParameterTypes()))
              .filter(Predicate.not(Method::isVarArgs))
              .forEach(method -> {
                  final var operator = new UnaryOperator() {
                      @Override
                      protected double eval(final double a)
                              throws
                              InvocationTargetException,
                              IllegalAccessException,
                              IllegalArgumentException {
                          final var validator =
                                  operatorValidators.getOrDefault(method.getName().toLowerCase(),
                                                                  (unused) -> true);
                          if (Double.isFinite(a))
                              if (validator.test(new double[]{a}))
                                  return (double) method.invoke(null, a);
                          throw new IllegalArgumentException(
                                  "At least one parameter does not match requirements for " +
                                  "operator");
                      }
                  };
                  opsMap.putIfAbsent(method.getName().toLowerCase(), operator);
              });
    }

    /**
     * This method initializes the binary operators.
     *
     * @param opsMap The map that later should contain the operators
     */
    private static void initializeBinaryOperators(final ConcurrentMap<String, Operator> opsMap) {
        Arrays.stream(Math.class.getMethods())
              .filter(m -> Modifier.isStatic(m.getModifiers()))
              .filter(m -> Arrays.equals(new Class[]{double.class, double.class},
                                         m.getParameterTypes()))
              .filter(Predicate.not(Method::isVarArgs))
              .forEach(method -> {
                  final var operator = new BinaryOperator() {
                      @Override
                      protected double eval(final double a, final double b)
                              throws
                              InvocationTargetException,
                              IllegalAccessException,
                              IllegalArgumentException {
                          final var validator =
                                  operatorValidators.getOrDefault(method.getName().toLowerCase(),
                                                                  (unused) -> true);
                          if (Double.isFinite(a) && Double.isFinite(b))
                              if (validator.test(new double[]{a, b}))
                                  return (double) method.invoke(null, a, b);
                          throw new IllegalArgumentException(
                                  "At least one parameter does not match requirements for " +
                                  "operator");
                      }
                  };
                  opsMap.putIfAbsent(method.getName().toLowerCase(), operator);
              });
    }

    /**
     * This method takes an upnExpression and calculates the result.
     *
     * @param upnExpression the String with the operations and values to process.
     *
     * @return the results of the operations and values in the upnExpression.
     */
    public static double eval(final String upnExpression) {
        Objects.requireNonNull(upnExpression);
        if (upnExpression.isEmpty())
            throw new IllegalArgumentException("upnExpression must not be empty");

        final var stack = new Stack<Double>();
        final Consumer<String> tokenEvaluator = token -> {
            if (operators.containsKey(token)) {
                // It's an operation
                try {
                    operators.get(token).eval(stack);
                }
                catch (final InvocationTargetException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            else {
                try {
                    // It's probably a double, then.
                    stack.push(Double.parseDouble(token));
                }
                catch (final NumberFormatException nfe) {
                    // That was not a double! Oops.
                    final var msg = "Invalid token " + token + " in expression " + upnExpression;
                    throw new IllegalArgumentException(msg, nfe);
                }
            }
        };

        // Prepare and evaluate/execute all tokens
        SPLIT_PATTERN.splitAsStream(upnExpression)
                     .parallel()
                     .map(String::trim)
                     .filter(Predicate.not(String::isEmpty))
                     .map(String::toLowerCase)
                     .forEachOrdered(tokenEvaluator);

        if (stack.size() == 1)
            return stack.get(0);
        else
            throw new IllegalStateException(
                    "Expected single result for expression " + upnExpression + ", got " +
                    stack.size());
    }
}
