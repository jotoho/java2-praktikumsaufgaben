/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt6.operators;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Stack;

/**
 * This class is a template for binary operators.
 */
abstract public class UnaryOperator implements Operator {
    /**
     * {@inheritDoc}
     */

    public void eval(final Stack<Double> stack)
            throws IllegalArgumentException, InvocationTargetException, IllegalAccessException {
        if (stack == null)
            throw new IllegalArgumentException("stack must not be null");
        if (stack.size() < 1)
            throw new IllegalStateException("Unary operator requires one argument");

        // requireNonNull for readability
        stack.push(eval(Objects.requireNonNull(stack.pop())));
    }

    /**
     * @param a number that should be used in the operation.
     *
     * @return the result of the operation.
     *
     * @throws InvocationTargetException happens when we access methods and runtime says "Nope!"
     * @throws IllegalAccessException    happens when we access methods and runtime says "Nope!"
     * @throws IllegalArgumentException  happens when parameter does not match requirements for
     *                                   operator
     */
    abstract protected double eval(final double a)
            throws InvocationTargetException, IllegalAccessException, IllegalArgumentException;
}
