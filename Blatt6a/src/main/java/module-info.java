module BlattSix {
    requires transitive javafx.controls;

    opens de.jotoho.fhswf.java2.Blatt6 to javafx.graphics;
}
