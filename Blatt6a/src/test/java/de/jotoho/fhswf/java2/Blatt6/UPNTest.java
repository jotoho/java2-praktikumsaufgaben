/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt6;

import static de.jotoho.fhswf.java2.Blatt6.UPN.eval;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UPNTest {

    @Test
    @DisplayName("False input")
    void falseInput() {
        assertThrows(IllegalArgumentException.class, () -> eval("Unlimited pow er!!!"));
        assertThrows(IllegalStateException.class, () -> eval("1 2 3 4 +"));
        assertThrows(IllegalStateException.class, () -> eval("1 +"));
    }

    @Test
    @DisplayName("Add")
    void addition() {
        assertEquals(7, eval("3 4 +"));
    }

    @Test
    @DisplayName("Sub")
    void subtraction() {
        assertEquals(1, eval("5 4 -"));
    }

    @Test
    @DisplayName("Multi")
    void multiplication() {
        assertEquals(15, eval("3 5 *"));
    }

    @Test
    @DisplayName("Div")
    void division() {
        assertEquals(1, eval("5 5 /"));
    }

    @Test
    @DisplayName("Mod")
    void modulo() {
        assertEquals(1, eval("6 5 %"));
    }

    @Test
    @DisplayName("Sqrt")
    void sqrt() {
        assertEquals(5, eval("25 sqrt"));
        assertEquals(12, eval("144 SQRT"));
        assertThrows(IllegalArgumentException.class, () -> eval("-3 sqrt"));
        assertThrows(IllegalStateException.class, () -> eval("3 3 sqrt"));
    }

    @Test
    @DisplayName("Cbrt")
    void cbrt() {
        assertEquals(5, eval("125 cbrt"));
        assertThrows(IllegalArgumentException.class, () -> eval("-3 cbrt"));
        assertThrows(IllegalStateException.class, () -> eval("3 3 cbrt"));
    }

    @Test
    @DisplayName("Pow")
    void pow() {
        assertEquals(125, eval("5 3 pow"));
    }

    @Test
    @DisplayName("Abs")
    void abs() {
        assertEquals(125.5, eval("-125.5 abs"));
    }

    @Test
    @DisplayName("Acos")
    void acos() {
        assertEquals(0, eval("1 acos"));
        assertEquals(Math.PI, eval("-1 acos"));
    }

    @Test
    @DisplayName("Asin")
    void asin() {
        assertEquals(1.0 / 2.0 * Math.PI, eval("1 asin"));
        assertEquals(-Math.PI / 2.0, eval("-1 asin"));
    }

    @Test
    @DisplayName("Atan")
    void atan() {
        assertEquals(Math.PI / 4.0, eval("1 atan"));
        assertEquals(-Math.PI / 4.0, eval("-1 atan"));
    }

    @Test
    @DisplayName("Atan2")
    void atan2() {
        assertEquals(Math.PI / 4.0, eval("1 1 atan2"));
        assertEquals(3.0 * Math.PI / 4.0, eval("1 -1 atan2"));
    }

    @Test
    @DisplayName("Ceil")
    void ceil() {
        assertEquals(2.0, eval("1.4 ceil"));
        assertEquals(-1.0, eval("-1.4 ceil"));
    }

    @Test
    @DisplayName("CopySign")
    void copySign() {
        assertEquals(-7.0, eval("7 -2 copysign"));
        assertEquals(7.0, eval("7 2 copysign"));
    }

    @Test
    @DisplayName("Cos")
    void cos() {
        assertEquals(1.0, eval("0 cos"));
    }

    @Test
    @DisplayName("Sin")
    void sin() {
        assertEquals(0.0, eval("0 sin"));
    }

    @Test
    @DisplayName("Tan")
    void tan() {
        assertEquals(0.0, eval("0 tan"));
    }

    @Test
    @DisplayName("Cosh")
    void cosh() {
        assertEquals(1.0, eval("0 cosh"));
    }

    @Test
    @DisplayName("Exp")
    void exp() {
        assertEquals(1.0, eval("0 exp"));
    }

    @Test
    @DisplayName("Expm1")
    void expm1() {
        assertEquals(0.0, eval("0 expm1"));
    }

    @Test
    @DisplayName("Floor")
    void floor() {
        assertEquals(1.0, eval("1.5 floor"));
        assertEquals(-2.0, eval("-1.5 floor"));
    }

    @Test
    @DisplayName("Hypot")
    void hypot() {
        assertEquals(Math.sqrt(2.0), eval("1 1 hypot"));
    }

    @Test
    @DisplayName("IEEEremainder")
    void IEEEremainder() {
        assertEquals(-1.0, eval("3 2 ieeeremainder"));
        assertEquals(-2.0, eval("28 5 ieeeremainder"));
    }

    @Test
    @DisplayName("Log")
    void log() {
        assertEquals(0.0, eval("1 log"));
    }

    @Test
    @DisplayName("Log10")
    void log10() {
        assertEquals(2.0, eval("100 log10"));
    }

    @Test
    @DisplayName("Log1p")
    void log1p() {
        assertEquals(0.0, eval("0 log1p"));
    }

    @Test
    @DisplayName("Max")
    void max() {
        assertEquals(10.0, eval("10 5 max"));
    }

    @Test
    @DisplayName("Min")
    void min() {
        assertEquals(5.0, eval("10 5 min"));
    }

    @Test
    @DisplayName("NextAfter")
    void nextAfter() {
        assertEquals(9.999999999999998, eval("10.0 2.0 nextafter"));
        assertEquals(10.000000000000002, eval("10.0 12.0 nextafter"));
    }

    @Test
    @DisplayName("NextDown")
    void nextDown() {
        assertEquals(9.999999999999998, eval("10 nextdown"));
    }

    @Test
    @DisplayName("NextUp")
    void nextUp() {
        assertEquals(10.000000000000002, eval("10 nextup"));
    }

    @Test
    @DisplayName("Rint")
    void rint() {
        assertEquals(11, eval("10.9 rint"));
    }

    @Test
    @DisplayName("Signum")
    void signum() {
        assertEquals(-1, eval("-10 signum"));
        assertEquals(1, eval("10 signum"));
        assertEquals(0, eval("0 signum"));
    }

    @Test
    @DisplayName("Sinh")
    void sinh() {
        assertEquals(0, eval("0 sinh"));
    }

    @Test
    @DisplayName("Tanh")
    void tanh() {
        assertEquals(0, eval("0 tanh"));
    }

    @Test
    @DisplayName("ToDegrees")
    void toDegrees() {
        assertEquals(57.29577951308232, eval("1 todegrees"));
    }

    @Test
    @DisplayName("ToRadians")
    void toRadians() {
        assertEquals(Math.PI, eval("180 toradians"));
    }

    @Test
    @DisplayName("Ulp")
    void ulp() {
        assertEquals(2.220446049250313E-16, eval("1 ulp"));
    }
}
