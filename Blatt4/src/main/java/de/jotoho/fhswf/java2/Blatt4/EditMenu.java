/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/
package de.jotoho.fhswf.java2.Blatt4;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.Clipboard;
import javafx.scene.input.KeyCombination;

public class EditMenu extends Menu {
    private final TextInputControl textControl;
    private static final String COPY_ITEM_NAME = "C_opy";
    private static final KeyCombination COPY_KEY = KeyCombination.keyCombination("Shortcut+C");

    private static final String PASTE_ITEM_NAME = "_Paste";
    private static final KeyCombination PASTE_KEY = KeyCombination.keyCombination("Shortcut+V");

    private static final String CUT_ITEM_NAME = "_Cut";
    private static final KeyCombination CUT_KEY = KeyCombination.keyCombination("Shortcut+X");

    private static final String SELECT_ALL_ITEM_NAME = "_Select All";
    private static final KeyCombination SELECT_ALL_KEY = KeyCombination.keyCombination("Shortcut+A");


    public EditMenu(final String name, final TextInputControl textControl) {
        if (name == null)
            throw new NullPointerException("Name of EditMenu cannot be null");
        if (textControl == null)
            throw new NullPointerException("TextInputControl for EditMenu cannot be null");

        this.setText(name);
        this.textControl = textControl;

        final var copyItem = new EditMenuItem(COPY_ITEM_NAME, COPY_KEY, event -> this.textControl.copy());
        this.getItems().add(copyItem);

        final var pasteItem = new EditMenuItem(PASTE_ITEM_NAME, PASTE_KEY, event -> this.textControl.paste());
        this.getItems().add(pasteItem);

        final var cutItem = new EditMenuItem(CUT_ITEM_NAME, CUT_KEY, event -> this.textControl.cut());
        this.getItems().add(cutItem);

        final var selectAllItem = new EditMenuItem(SELECT_ALL_ITEM_NAME, SELECT_ALL_KEY, event -> this.textControl.selectAll());
        this.getItems().add(selectAllItem);

        this.setOnShown(event -> {
            try {
                pasteItem.setDisable(!Clipboard.getSystemClipboard().hasString());
                copyItem.setDisable(textControl.getSelectedText().isEmpty());
                cutItem.setDisable(textControl.getSelectedText().isEmpty());
                selectAllItem.setDisable(textControl.getText().isEmpty());
            } catch (final Throwable e) {
                final Alert alert = new Alert(Alert.AlertType.ERROR, "Unknown error:\n\r" + e.getMessage(), ButtonType.OK);
                alert.setResizable(true);
                alert.showAndWait();
            }
        });
    }
}
