/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/

package de.jotoho.fhswf.java2.Blatt4;

import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

/**
 * Generates and places the elements of the text editor application.
 *
 * @author Tim Beckmann {@literal <beckmann.tim@fh-swf.de>}
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 * */
public class TextEditorBorderPane extends BorderPane {
    /**
     * @param countdownTime The exit countdown to pass to the {@link CountdownExitButton} at the top.
     *                      Must meet the requirements of {@link CountdownExitButton#CountdownExitButton(short, Runnable)}
     * @throws IllegalArgumentException If the countdownTime parameter is invalid.
     * */
    public TextEditorBorderPane(final short countdownTime) {
        if (!CountdownExitButton.isValidDuration(countdownTime))
            throw new IllegalArgumentException("Invalid countdown duration.");

        final var textEditor = new TextField();
        setCenter(textEditor);

        final var menuBar = new ApplicationMenuBar(textEditor);
        menuBar.setMaxWidth(Double.MAX_VALUE);
        setTop(menuBar);

        final var exitButton = new CountdownExitButton(countdownTime, null);
        exitButton.setMaxWidth(Double.MAX_VALUE);
        setBottom(exitButton);
    }
}
