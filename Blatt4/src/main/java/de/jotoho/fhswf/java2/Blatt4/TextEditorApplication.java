/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/

package de.jotoho.fhswf.java2.Blatt4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.util.Objects;

/**
 * A basic "text editor" with a user-unfriendly button that simply closes the application after a while.
 *
 * <p>
 * This application features a window containing a basic main menu, a text field
 * and a button that closes the application when triggered or after a time.
 * </p>
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 * @author Tim Beckmann {@literal <beckmann.tim@fh-swf.de>}
 */
public class TextEditorApplication extends Application {
    /**
     * A main method that launches this application. Do not call manually.
     *
     * @param args The CLI arguments. Must not be null!
     */
    public static void main(final String[] args) {
        launch(Objects.requireNonNull(args));
    }

    /**
     * Loads {@link TextEditorBorderPane} into the application's main window.
     *
     * @see Application#start(Stage)
     */
    @Override
    public void start(final Stage primaryStage) {
        try {
            final var editor = new TextEditorBorderPane((short) 100);
            final var scene = new Scene(editor);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (final Throwable e) {
            final Alert alert = new Alert(AlertType.ERROR, e.getMessage(), ButtonType.OK);
            alert.setResizable(true);
            alert.showAndWait();
        }
    }
}
