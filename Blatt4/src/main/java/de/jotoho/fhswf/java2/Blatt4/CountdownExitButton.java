/*
    BSD 3-Clause License

    Copyright (c) 2023 Jonas Tobias Hopusch & Tim Beckmann

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

    SPDX-License-Identifier: BSD-3-Clause
*/

package de.jotoho.fhswf.java2.Blatt4;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.util.Duration;

/**
 * A button that runs an exit command after a given amount of time has passed,
 * or it is pressed.
 *
 * <p>
 *     The action to take and the time to wait before activating it, are configurable.
 * </p>
 *
 * @author Jonas Tobias Hopusch {@literal <hopusch.jonastobias@fh-swf.de>}
 * @author Tim Beckmann {@literal <beckmann.tim@fh-swf.de>}
 * */
public class CountdownExitButton extends Button {
    private static final short DEFAULT_TIME = 10;
    private static final Runnable DEFAULT_EXIT_ACTION = Platform::exit;
    private static final String BUTTON_TEXT = "Exit";
    private final Runnable exitAction;
    private final Timeline timeline = new Timeline();
    private short remainingSeconds;

    /**
     * Checks if this class would accept the given countdown duration as a
     * constructor parameter.
     *
     * @see #CountdownExitButton(short, Runnable)
     * @return true if valid, false if invalid
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isValidDuration(final short countdown) {
        return countdown > 0;
    }

    /**
     * The timer begins to tick down immediately.
     *
     * @param countdownInSeconds The number of seconds to wait before triggering the exit action without
     *                           of user interaction. Value must be greater than zero.
     * @param exitAction The action(s) to take once the button has been pressed or the timer has expired.
     *                   If this parameter is null, the button calls {@link Platform#exit()}.
     * @throws IllegalArgumentException If parameter countdownInSeconds does not have a valid value.
     * */
    public CountdownExitButton(final short countdownInSeconds, final Runnable exitAction) {
        if (!isValidDuration(countdownInSeconds))
            throw new IllegalArgumentException("Countdown for CountDownExitButton must be > 0");

        this.remainingSeconds = countdownInSeconds;
        this.exitAction = exitAction != null ? exitAction : DEFAULT_EXIT_ACTION;
        this.updateButtonText();

        this.timeline.setCycleCount(Timeline.INDEFINITE);
        this.timeline
            .getKeyFrames()
            .add(new KeyFrame(Duration.seconds(1.0), event -> {
                this.updateButtonText();
                if (this.remainingSeconds == 0)
                    this.exitAction.run();
                else
                    this.remainingSeconds--;
            }));
        this.timeline.playFromStart();
    }

    /**
     * Calls the other constructor by setting a default wait time and the exit action to {@link Platform#exit()}
     *
     * @see #CountdownExitButton(short, Runnable)
     * */
    public CountdownExitButton() {
        this(DEFAULT_TIME, DEFAULT_EXIT_ACTION);
    }

    /**
     * Updates the text label of this button by appending the remaining seconds in square brackets
     * to {@link #BUTTON_TEXT}
     * */
    private void updateButtonText() {
        this.setText(BUTTON_TEXT + " [" + this.remainingSeconds + "]");
    }

    /**
     * This method is called when the button is pressed by the user.
     * (Though it could also be called programmatically)
     *
     * <p>
     *     It cancels the timer and triggers the exit action immediately.
     * </p>
     *
     * @see Button#fire()
     * */
    @Override
    public void fire() {
        super.fire();
        this.timeline.stop();
        this.exitAction.run();
    }

    /**
     * Cancels the running countdown.
     * */
    public void cancel() {
        this.timeline.stop();
    }
}
